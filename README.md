# Приложение `TicTacToe`

## URL проекта

`http://178.72.89.213:5050` или `http://178.72.89.213:5000`

Проект развёрнут на сервере Raspberry Pi 4 (OS Ubuntu) в Docker-контейнере

## Описание инфраструктуры проекта

Docker, docker-compose

### Техническое описание

Веб-сервер - `Nginx`
Frontend - `React`
Backend - `Express`
DB - `PostgreSQL`

## Описание frontend части

`./frontend/README.md`

## Описание backend части

`./backend/README.md`

## Автор: Ерешко Данил

Telegram: `@ereshkodanil`

Email: `ereshko.d.r@gmail.com`
