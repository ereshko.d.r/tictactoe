import BaseHandler from "../../modules/baseHandler.js";
import { knex } from "../../../App.js";
import { bcrypt } from "../../../App.js";

class UsersHandler extends BaseHandler {
    constructor() {
        super(['GET', 'POST'])
    }

    customRouters() {
        this.router.get('/block/:id', async (request, response) => this.blockUser(request, response))
        this.router.get('/unblock/:id', async (request, response) => this.unblockUser(request, response))
    }

    async getUser(id) {
        return knex('users')
            .select('*')
            .where('id', id).first()
            .then(data => {return data})
    }

    async blockUser(request, response) {
        const user = await this.getUser(request.params.id)
        if (!user) {
            console.log(`user with id ${request.params.id} not found`)
            return response.sendStatus(this.STATUS_CODE.NOT_FOUND)
        }
        knex('users')
            .update('status_active', false)
            .where('id', user.id).returning('status_active')
            .then((data) => {return response.status(this.STATUS_CODE.OK).json(data[0])})
            .catch(() => {return response.sendStatus(this.STATUS_CODE.BAD_REQUEST)})
    }

    async unblockUser(request, response) {
        const user = await this.getUser(request.params.id)
        if (!user) {
            console.log(`user with id ${request.params.id} not found`)
            return response.sendStatus(this.STATUS_CODE.NOT_FOUND)
        }
        knex('users')
            .update('status_active', true)
            .where('id', user.id).returning('status_active')
            .then((data) => {return response.status(this.STATUS_CODE.OK).json(data[0])})
            .catch(() => {return response.sendStatus(this.STATUS_CODE.BAD_REQUEST)})
    }

    async get(request, response) {
        return knex('users')
            .select('*')
            .then((users) => {
                return response.status(this.STATUS_CODE.OK).json(users)
            })
    }

    async create(request, response) {
        try {
            if (request.user.role === 'admin') {
                knex.transaction((trx) => {
                    knex('users').transacting(trx)
                        .insert(request.body).returning(['id', 'first_name'])
                        .then(arrayData => {return arrayData[0]})
                        .then(async (userData) => {
                            const pass = await bcrypt.hash(userData.first_name, 10)
                            knex('accounts').transacting(trx).insert({user_id: userData.id, login: userData.first_name, password: pass})
                        })
                    .then(trx.commit)
                    .then(response.sendStatus(this.STATUS_CODE.OK))
                    .catch(() => {
                        trx.rollback
                        response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
                    })
                })
            }
            else {
                return response.sendStatus(this.STATUS_CODE.FORBIDDEN)
            }
        }
        catch(error) {
            return response.sendStatus(this.STATUS_CODE.BAD_REQUEST)
        }
        
    }
}

export default new UsersHandler
