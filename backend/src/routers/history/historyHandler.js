import BaseHandlers from "../../modules/baseHandler.js"
import { knex } from "../../../App.js"

class HistoryHandler extends BaseHandlers {
    constructor() {
        super(['GET'])
    }

    async get(request, response) {
        const user = request.user
        const history = await knex.raw(`
            SELECT  g.*,
                    u1.first_name AS player1_first_name, u1.last_name AS player1_last_name, u1.second_name AS player1_second_name,
                    u2.first_name AS player2_first_name, u2.last_name AS player2_last_name, u2.second_name AS player2_second_name
            FROM games AS g
            JOIN users AS u1 ON g.player1_id = u1.id
            JOIN users AS u2 ON g.player2_id = u2.id
            WHERE u1.id = ${user.id} OR u2.id = ${user.id}
            ORDER BY g.started_at DESC
            LIMIT 100
            `)
        .then((data) => {
            return data.rows
        })
        response.status(this.STATUS_CODE.OK).json(history)
    }
}

export default new HistoryHandler