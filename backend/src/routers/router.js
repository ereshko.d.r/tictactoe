import express from 'express'

import eventsStream from '../middleware/eventsStream.js'
import checkJWT from '../middleware/checkJWS.js'
import logDateTimeRequest from '../middleware/logDateTimeRequest.js'

import GameHandler from './game/gameHandler.js'
import RatingHandler from './rating/ratingHandler.js'
import UsersHandler from './users/UsersHandler.js'
import AuthHandler from './auth/authHandler.js'
import HistoryHandler from './history/historyHandler.js'


const router = express.Router()

router.use(logDateTimeRequest)
router.use(checkJWT)
router.use('/events/', eventsStream)

router.use('/game', GameHandler)
router.use('/auth', AuthHandler)
router.use('/users', UsersHandler)
router.use('/rating', RatingHandler)
router.use('/history', HistoryHandler)

export default router

// import ActiveHandler from './acitve/activeHandler.js'
// router.use('/active', ActiveHandler)