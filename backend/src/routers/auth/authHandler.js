import BaseHandlers from "../../modules/baseHandler.js"
import { knex } from "../../../App.js"
import { bcrypt } from "../../../App.js"
import { jwt } from "../../../App.js"
import config from "../../../config.js"


class AuthHandler extends BaseHandlers {
    constructor() {
        super(['POST'])
        this.user = undefined
    }

    async checkPass(login, password) {
        const userData = await knex('users')
            .join('accounts', {'users.id': 'accounts.user_id'})
            .select('accounts.login', 'accounts.password', 'accounts.user_id', 'users.status_active')
            .where('login', login)
        if (userData.length === 0) {
            return false
        }
        if (!userData[0].status_active) {
            return false
        }
        if (bcrypt.compare(password, userData[0].password)) {
            const token = await this.createToken(userData[0].user_id)
            return token
        }
        return false
    }

    async createToken(user_id) {
        this.user = await knex('users')
                .select('*')
                .where('id', user_id).first()
        const token = jwt.sign({...this.user}, config.SECRET_KEY)
        return token
    }

    async create(request, response) {
        try {
            const {login, password} = request.body
            const result = await this.checkPass(login, password)
            if (result) {
                return response.status(this.STATUS_CODE.OK).json({
                    accessToken: result,
                    id: this.user.id,
                    last_name: this.user.last_name,
                    first_name: this.user.first_name,
                    second_name: this.user.second_name,
                    role: this.user.role
                })
            }
            return response.sendStatus(this.STATUS_CODE.NOT_FOUND)
        }
        catch (error) {
            return response.sendStatus(this.STATUS_CODE.BAD_REQUEST)
        }
    }
}

export default new AuthHandler