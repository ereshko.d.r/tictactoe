import BaseHandlers from "../../modules/baseHandler.js"


class ActiveHandler extends BaseHandlers {
    constructor() {
        super()
    }
}

export default new ActiveHandler
