import BaseHandler from "../../modules/baseHandler.js"
import { knex } from "../../../App.js"


class RatingHandler extends BaseHandler {
    constructor() {
        super(['GET'])
    }

    async get(request, response) {
        return knex.raw(`
                SELECT user_id,
                    users.last_name,
                    users.first_name,
                    users.second_name,
                    
                    SUM(wins_count + loses_count + draws_count) as total,
                    SUM(wins_count) as wins_count,
                    SUM(loses_count) as loses_count,
                    SUM(draws_count) as draws_count
                FROM "rating"
                    JOIN users ON users.id = rating.user_id
                WHERE month >= date_trunc('month', now()) - INTERVAL '6 month'
                GROUP BY user_id, last_name, first_name, second_name
                HAVING SUM(wins_count + loses_count + draws_count) > 10
                ORDER BY SUM(
                      wins_count * (CASE WHEN symbol = 'X' THEN 0.9 ELSE 1 END)
                    - loses_count * (CASE WHEN symbol = 'X' THEN 1.1 ELSE 1 END)
                    + draws_count * 0.25) / (COUNT(DISTINCT month)) DESC
                LIMIT 100
            `)
            .then((data) => {
                return data.rows
            })
            .then((data) => {
                return response.status(200).json(data)
            })
            .catch((error)=>console.log(error))
    }
}

export default new RatingHandler