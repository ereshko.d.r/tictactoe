import TicTacToeLogic from "./gameLogic.js"
import { listeningСlients } from "../../../App.js"
import { knex } from "../../../App.js"
import checkWSJWT from "../../middleware/checkWSJWT.js"

/**
 * Class that create new lobby for playing TicTacToe.
 * 
 * Has `constructor` that gets players and websocket, then gives game symbols for players
 * After that is done new record about game and create new websocket connection which has
 * addres like the `id` of record
 *
 * has middleware that checks for incoming connections
 * 
 * has handlers for handling players moves
 * 
 */

class GameServer {
    CONNECTION = 'connection'
    DISCONNECT = 'disconnect'

    GET_SATE = 'get state'
    MAKE_MOVE = 'make move'
    MESSAGE = 'message'
    UPDATE_STATE = 'update state'
    USERS = 'users'

    CROSS = 'x'
    ZERO = 'o'

    constructor(user1, user2, io) {
        this.timeID = setInterval(() => {this.#tick()}, 1000)
        this.#chosePlayerSymbols(user1, user2)
        this.#init(io)
    }

    async #init(io) {
        this.gameId = await this.#createRecordAboutGame()
        this.server = io.of(`/${this.gameId}`)
        this.game = new TicTacToeLogic(this.PLAYER_X, this.PLAYER_O)
        listeningСlients.sendRedirectToClients(this.PLAYER_X, this.PLAYER_O, this.gameId)
        this.server
            .use(checkWSJWT)
            .use((socket, next) => {
                const clientId = socket.data.user.id
                if (clientId != this.PLAYER_X.id && clientId != this.PLAYER_O.id) {
                    next(socket.disconnect())
                }
                next()
            })
            .on(this.CONNECTION, (socket) => {
                listeningСlients.setStatusInGame(socket.data.user)
                socket.emit(this.USERS, [this.PLAYER_X, this.PLAYER_O])
                socket.on(this.MESSAGE, (message) => {this.#messageHandler(message)})
                socket.on(this.GET_SATE, () => {this.sendUpdateState(socket)})
                socket.on(this.MAKE_MOVE, (index) => {this.#move(index)})
                socket.on(this.DISCONNECT, () =>
                    listeningСlients.setStatusWaiting(socket.data.user)
                )
            })
    }
    
    async #recordStartGame() {
        await knex('games')
            .where({id: this.gameId})
            .update({started_at: knex.fn.now()})
    }
    
    async #recordEndGame(gameState) {
        let result
        if (gameState.symbol === this.CROSS) {
            result = true
        }
        else if (gameState.symbol === this.ZERO) {
            result = false
        }
        else {
            result = null
        }
        await knex('games')
            .where({id: this.gameId})
            .update({
                ended_at: knex.fn.now(),
                game_result: result
            })
        this.#updateRating()
    }
    
    async #recordChat(data) {
        const parsedData = JSON.parse(data)
        const userId = parsedData.user.id
        const message = parsedData.text
        await knex('messenger')
            .insert({
                game_id: this.gameId,
                user_id: userId,
                message: message
            })
    }
    
    async #createRecordAboutGame() {
        return knex('games')
            .insert({
                player1_id: this.PLAYER_X.id,
                player2_id: this.PLAYER_O.id
            })
            .returning('id')
            .then((arrayData) => {return arrayData[0].id})
        }

    async #updateRating() {
        knex.raw(`
            REFRESH MATERIALIZED VIEW concurrently rating;
        `).catch(error => console.log(error))
    }
        
        
    #chosePlayerSymbols(user1, user2) {
        if (Math.random() >= 0.5) {
            user1.symbol = this.CROSS
            user2.symbol = this.ZERO
            this.PLAYER_X = user1
            this.PLAYER_O = user2
        }
        else {
            user2.symbol = this.CROSS
            user1.symbol = this.ZERO
            this.PLAYER_X = user2
            this.PLAYER_O = user1
        }
    }
    
    #tick() {
        if (this.game.isGameStarted && !this.game.isGameOver) {
            this.game.time++
            this.sendUpdateState()
        }
    }

    #messageHandler(message) {
        this.#recordChat(message)
        this.server.emit(this.MESSAGE, message)
    }

    #move(index) {
        try {
            const result = this.game.game(index)
            if (this.game.emptyFields === 8) {
                
                this.#recordStartGame()
            }
            if (result) {
    
                this.#recordEndGame(result.winner)
            }
            this.sendUpdateState()
        }
        catch(error) {
            console.log(error)
        }
    }

    sendUpdateState(socket) {
        if (socket) {
            socket.emit(this.UPDATE_STATE, this.game.gameState)
        }
        else {
            this.server.emit(this.UPDATE_STATE, this.game.gameState)
        }
    }
}

export default GameServer