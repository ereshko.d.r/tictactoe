export class NotEmptyFieldError extends Error {
    constructor(symbol, numOfField) {
        const message = `You can't put "${symbol}" on filed "${numOfField}"`;
        super(message);
        this.name = "NotEmptyFieldError";
        this.numOfField = numOfField;
    }
}

export class InputTypeError extends Error {
    constructor(numOfField) {
        const message = `Incoming type must be "Number", not ${numOfField}}`;
        super(message);
        this.name = "InputTypeError";
        this.numOfField = numOfField;
    }
}

export class IncorrectValueError extends Error {
    constructor(numOfField) {
        const message = `Incoming value must be greater or equal zero and less than nine, not ${numOfField}`;
        super(message);
        this.name = "IncorrectValueError";
        this.numOfField = numOfField;
    }
}

export default class TicTacToeLogic {
    field = [
        null, null, null,
        null, null, null,
        null, null, null,
    ];
    emptyFields = 9;
    
    constructor(PLAYER_X, PLAYER_O) {
        this.PLAYER_X = PLAYER_X;
        this.PLAYER_O = PLAYER_O;
        this.DRAW = 'draw';

        this.isGameOver = false;
        this.currentPlayer = this.PLAYER_X;
        this.winner = false
        this.combination = false
        this.time = 0

    }

    checkIsEmptyField(numOfField) {
        return this.field[numOfField] === null;
    }

    get gameState() {
        return {
            field: this.field,
            isGameOver: this.isGameOver,
            currentPlayer: this.currentPlayer,
            winner: this.winner,
            combination: this.combination,
            emptyFields: this.emptyFields,
            time: this.time
        }
    }

    set gameState(state) {
        this.field = state.field
        this.isGameOver = state.isGameOver
        this.currentPlayer = state.currentPlayer
        this.winner = state.winner
        this.combination = state.combination
        this.emptyFields = state.emptyFields
        this.time = state.time
    }

    #putElement(symbol, numOfField) {
        if (isNaN(Number(numOfField))) {
            throw new InputTypeError(numOfField);
        }
        else if (numOfField < 0 || numOfField > 8) {
            throw new IncorrectValueError(numOfField);
        }
        else if (!this.checkIsEmptyField(numOfField)) {
            throw new NotEmptyFieldError(symbol, numOfField);
        }
        this.field[numOfField] = symbol;
        this.emptyFields--;
        return true;
    }

    #compareCount(symbolX, symbolO) {
        if (symbolX === 3) {
            return this.PLAYER_X;
        }
        if (symbolO === 3) {
            return this.PLAYER_O;
        }
        return false
    }
    
    #resultObject(winner, combination = null) {
        const result = {
            winner: winner,
            combination: combination,
        }
        this.winner = winner
        this.combination = combination
        return result;
    }

    #checkHorisontalCombinations() {
        let symbolX;
        let symbolO;
        let combination;
        let resultCompareCount;
        for (let i = 0; i <= 8; i++) {
            if (i % 3 === 0) {
                symbolX = 0;
                symbolO = 0;
                combination = [];
            }
            switch (this.field[i]) {
                case this.PLAYER_X.symbol:
                    symbolX++;
                    break;
                case this.PLAYER_O.symbol:
                    symbolO++;
                    break;
                default:
                    break;
            }
            combination.push(i)
            resultCompareCount = this.#compareCount(symbolX, symbolO)
            if (resultCompareCount) {
                return this.#resultObject(resultCompareCount, combination);
            }
        }
        return false;
    }

    #checkVerticalCombinations() {
        let symbolX = 0;
        let symbolO = 0;
        let resultCompareCount;
        let combination = []
        for (let i = 0; i <= 2; i++) {
            for (let k = i; k <= i + 6; k+=3) {
                switch (this.field[k]) {
                    case this.PLAYER_X.symbol:
                        symbolX++;
                        break;
                    case this.PLAYER_O.symbol:
                        symbolO++;
                        break;
                    default:
                        break;
                }
                combination.push(k);
            }
            resultCompareCount = this.#compareCount(symbolX, symbolO)
            if (resultCompareCount) {
                return this.#resultObject(resultCompareCount, combination);
            }
            symbolX = 0;
            symbolO = 0;
            combination = [];
        }
        return false;
    }

    #checkDiagonalCombitations() {
        let symbolX = 0;
        let symbolO = 0;
        let resultCompareCount;
        let combination = []
        for (let i = 0; i <= 8; i+=4) {
            switch (this.field[i]) {
                case this.PLAYER_X.symbol:
                    symbolX++;
                    break;
                case this.PLAYER_O.symbol:
                    symbolO++;
                    break;
                default:
                    break;
            }
            combination.push(i)
        }
        resultCompareCount = this.#compareCount(symbolX, symbolO);
        if (resultCompareCount) {
            return this.#resultObject(resultCompareCount, combination);
        }
        symbolX = 0;
        symbolO = 0;
        combination = [];
        for (let i = 2; i <= 6; i+=2) {
            switch (this.field[i]) {
                case this.PLAYER_X.symbol:
                    symbolX++;
                    break;
                case this.PLAYER_O.symbol:
                    symbolO++;
                    break;
                default:
                    break;
            }
            combination.push(i)
        }
        resultCompareCount = this.#compareCount(symbolX, symbolO);
        if (resultCompareCount) {
            return this.#resultObject(resultCompareCount, combination);
        }
        return false;
    }

    #checkRules() {
        let resultCheckVerticalCombinations = this.#checkVerticalCombinations();
        let resultChecklHorisontalCombinations = this.#checkHorisontalCombinations();
        let resultCheckDiagonalCombinations = this.#checkDiagonalCombitations();
        if (resultCheckVerticalCombinations) {
            this.isGameOver = true;
            return resultCheckVerticalCombinations;
        }
        else if (resultChecklHorisontalCombinations) {
            this.isGameOver = true;
            return resultChecklHorisontalCombinations;
        }
        else if (resultCheckDiagonalCombinations) {
            this.isGameOver = true;
            return resultCheckDiagonalCombinations;
        }
        else if (this.emptyFields === 0) {
            this.isGameOver = true;
            return this.#resultObject(this.DRAW);
        }
        return false;
    }

    #changePlayerMove() {
        if (this.currentPlayer === this.PLAYER_X) {
            this.currentPlayer = this.PLAYER_O;
        }
        else {
            this.currentPlayer = this.PLAYER_X;
        }
    }

    isEmptyFields () {
        return this.emptyFields === 9;
    }

    get isGameStarted () {
        return this.emptyFields < 9
    }
    
    game(numOfField) {
        this.#putElement(this.currentPlayer.symbol, numOfField);
        const afterMove = this.#checkRules();
        if (afterMove) {
            this.isGameOver = true;
            return afterMove;
        }
        this.#changePlayerMove();
        return false;
    }

    restart() {
        this.field = [
            null, null, null,
            null, null, null,
            null, null, null,
        ];
        this.emptyFields = 9;
        this.isGameOver = false;
        this.currentPlayer = this.PLAYER_X;
        this.winner = false;
        this.combination = false
        this.time = 0
    }
}
