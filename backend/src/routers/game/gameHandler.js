import BaseHandlers from "../../modules/baseHandler.js"
import { listeningСlients } from "../../../App.js"
import { knex } from "../../../App.js"
import { ws } from "../../../App.js"
import GameServer from "./gameServer.js"

class GameHandler extends BaseHandlers {
    constructor() {
        super(['GET', 'POST'])
    }

    
    customRouters() {
        this.router.get('/invite/:id', async (request, response) => this.invite(request, response))
    }
    
    /**
     * class method that send invite to user for playing
     * 
     * @param {*} request 
     * @param {*} response 
    */
   
    async getWinrate(user) {
        return knex.raw(`
            SELECT 
                SUM(wins_count::float) / SUM(wins_count + loses_count + draws_count) as precent
            FROM rating
            WHERE user_id = ${user.id}
        `)
        .then((data) => {
            return Math.floor(data.rows[0].precent * 100)
        })
    }

    async invite(request, response) {
        const user_id = request.params.id
        const user = await knex('users').select('*').where('id', user_id).first()
        if (!user) {
            response.sendStatus(this.STATUS_CODE.NOT_FOUND)
        }
        listeningСlients.sendInviteToUser(user, request.user)
        response.sendStatus(this.STATUS_CODE.OK)
    }

    async create(request, response) {
        const [user1, user2] = request.body
        user1.winRate = await this.getWinrate(user1)
        user2.winRate = await this.getWinrate(user2)
        new GameServer(user1, user2, ws)
        response.sendStatus(this.STATUS_CODE.OK)
    }
}

export default new GameHandler