import express from "express"


/**
 * Base handler class
 * 
 * it is direct request to class methods, which described below
 * 
 *  /endpoint/      GET     Get all records             get()
 * 
 *  /endpoint/      POST    Create new record           create()
 * 
 *  /endpoint/:id/  GET     Get record details          details()
 * 
 *  /endpoint/:id/  PUT     Update record               update()
 * 
 *  /endpoint/:id/  PATCH   Update partial record       updatePartial()
 * 
 *  /endpoint/:id/  DELETE  Delete record               delete()
 * 
 */

class BaseHandlers {
    /*
    /endpoint/      GET     Get all records             get()
    /endpoint/      POST    Create new record           create()
    /endpoint/:id/  GET     Get record details          details()
    /endpoint/:id/  PUT     Update record               update()
    /endpoint/:id/  PATCH   Update partial record       updatePartial()
    /endpoint/:id/  DELETE  Delete record               delete()
    */
    STATUS_CODE = {
        OK:                     200,
        CREATED:                201,
        ACCEPTED:               202,
        NO_CONTENT:             204,

        MOVED_PERMAMENTLY:      301,
        FOUND:                  302,

        BAD_REQUEST:            400,
        UNAUTHORIZED:           401,
        FORBIDDEN:              403,
        NOT_FOUND:              404,
        NOT_ALLOWED_METHOD:     405,

        INTERNAL_SERVER_ERROR:  500
    }

    /**
     * it is allowing request methods for handlering
     * 
     * required param. If you don't use it, handler won't accept requests
     * 
     * used methods: [`'GET'`, `'POST'`, `'PUT`, `'PATCH'`, `'DELETE'`]
     * @param {Array} allowedMethods 
     * @returns 
     */

    constructor(allowedMethods) {
        this.allowedMethods = allowedMethods || []
        this.router = express.Router()
        this.router.use((request, response, next) => {
            if (!this.#checkAllowedMethods(request.method)) {
                return response.sendStatus(this.STATUS_CODE.NOT_ALLOWED_METHOD)
            }
            next()
        })
        this.customMiddleware()
        this.defaultRouters()
        this.customRouters()
        return this.router
    }

    /**
     * middleware mehtod. Checking the method if an incomig request to be allowed
     * 
     * else return response with 405 status code
     * @param {request.method} method 
     * @returns {boolean}
     */

    #checkAllowedMethods(method) {
        return this.allowedMethods.includes(method)
    }

    defaultRouters() {
        this.router.get('/', async (request, response) => this.get(request, response))
        this.router.post('/', async (request, response) => this.create(request, response))
        this.router.get('/:id', async (request, response) => this.details(request, response))
        this.router.put('/:id', async (request, response) => this.update(request, response))
        this.router.patch('/:id', async (request, response) => this.updatePartial(request, response))
        this.router.delete('/:id', async (request, response) => this.deltete(request, response))
    }

    /**
     * method for adding new handlers or routers
     */

    customRouters() {

    }

    /**
     * method for adding new middleware
     */

    customMiddleware() {

    }

    /**
     * default class method for handling `GET` requests
     * /endpoint/      `GET`
     */

    async get(request, response) {
    }

    /**
     * default class method for handling `POST` requests
     * /endpoint/      `POST`
     */

    async create(request, response) {
    }

    /**
     * default class method for handling `GET` requests about details
     * /endpoint/:id   `GET`
     */

    async details(request, response) {
    }

    /**
     * default class method for handling `PUT` requests for updating
     * /endpoint/:id   `PUT`
     */

    async update(request, response) {
    }

    /**
     * default class method for handling `PATCH` requests for partial updating
     * /endpoint/:id   `PATCH`
     */

    async updatePartial(request, response) {
    }

    /**
     * default class method for handling `PATCH` requests for deliting
     * /endpoint/:id   `DELETE`
     */

    async deltete(request, response) {
    }
   
}

export default BaseHandlers