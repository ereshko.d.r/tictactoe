import { jwt } from "../../App.js"
import config from "../../config.js"


/**
*   Event sourse class.
*   is adding client to `clients list` and sending some events to clients.
*   
*   For example: send invite to client for playing game
*/

class Clients {

    list = []
    
    /**
     * is adding client to `clients list`
     * @param {object} user 
     * @param {object} response 
    */

    addClient = (user, response) => {
        if (this.findClient(user) === -1) {
            console.log(`EVENT-SOURCE:    id:${user.id} has been connected`)
            const client = user
            client.status_game = true
            const newClient = {
                user: client,
                response
            }
            this.list.push(newClient)
            this.updateActiveClients()
        }
    }

    /**
     * return object of user with its `response`
     * @param {object} user 
     * @returns {{user, response}} object of client list
     */

    getClient(user) {
        const recipientId = this.findClient(user)
        return this.list[recipientId]
    }

    /**
     * send fresh state about users
     */

    updateActiveClients() {
        const users = []
        for (const client of this.list) {
            users.push(client.user)
        }
        const data = `data: ${JSON.stringify({users: users})}\n\n`
        if (this.list.length > 0) {
            this.list.forEach(client => client.response.write(data))
        }
    }

    /**
     * remove client from clients list
     * @param {object} user 
     */

    removeClient = (user) => {
        const userId = this.findClient(user)
        this.list.splice(userId, 1)
        console.log(`EVENT-SOURCE:    id:${user.id} has been disconnected`)
        this.updateActiveClients()
    }

    /**
     * find client by `object` and return its index clients list
     * @param {object} user 
     * @returns {number} userId
     */

    findClient(user) {
        return this.list.findIndex((obj) => user.id === obj.user.id)
    }

    /**
     * find client by `JWJ` and return its index clients list 
     * @param {string} token 
     * @returns {number} userId
     */

    async findClientByJWT(token) {
        const secret = config.SECRET_KEY
        const userId = await jwt.verify(token, secret, (error, user) => {
                if (error) {
                    return false
                }
                return this.findClient(user)
            })
        return userId
    }

    /**
     * set user `status game` to `in game`
     * @param {object} user user
     */

    setStatusInGame(user) {
        const recipientId = this.findClient(user)
        this.list[recipientId].user.status_game = false
        this.updateActiveClients()
    }

    /**
     * set user `status game` to `ready to play`
     * @param {object} user 
     */

    setStatusWaiting(user) {
        const recipientId = this.findClient(user)
        this.list[recipientId].user.status_game = true
        this.updateActiveClients()
    }



    /**
     * Sending invite `request` to user
     * @param {object} user to user
     * @param {object} sender from user
     */

    sendInviteToUser(user, sender) {
        const recipientId = this.findClient(user)
        const client = this.list[recipientId]
        const data = `data: ${JSON.stringify({invite: sender})}\n\n`
        client.response.write(data)
    }

    /**
     * sending link to game for players
     * @param {object} user1 player 1
     * @param {object} user2 player 2
     * @param {number} gameId 
     */

    sendRedirectToClients(user1, user2, gameId) {
        const client1 = this.getClient(user1)
        const client2 = this.getClient(user2)
        const data = `data: ${JSON.stringify({redirect: gameId})}\n\n`
        client1.response.write(data)
        client2.response.write(data)
    }
}

export default Clients