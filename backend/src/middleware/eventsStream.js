import { listeningСlients } from "../../App.js"

/**
 * middleware that subscribes clients to `events`
 * 
 * @param {*} request 
 * @param {*} response 
 * @param {*} next 
 */

const eventsStream = (request, response, next) => {
    const headers = {
        'Content-Type': 'text/event-stream',
        'Connection': 'keep-alive',
        'Cache-Control': 'no-cache'
    };
    response.writeHead(200, headers)
    const user = request.user
    listeningСlients.addClient(user, response)
    
    request.on('close', () => {
        listeningСlients.removeClient(user)
    });
    next()
}

export default eventsStream