const logDateTimeRequest = (request, response, next) => {
    const method = request.method
    const url = `${request.baseUrl}${request.path}`
    const dateTime = new Date().toLocaleString()
    console.log(`${method}:             ${url} - ${dateTime}`)
    next()
}

export default logDateTimeRequest