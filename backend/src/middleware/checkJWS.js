import { jwt } from '../../App.js'
import config from '../../config.js'

/**
 * middleware that checks the `JWT` with incoming requsts
 * 
 * if request don't have the `JWT` or `JWT` is not valid, that middleware resturn response with `403` status code
 * @param {*} request 
 * @param {*} response 
 * @param {*} next 
 * @returns response
 */

const checkJWT = async (request, response, next) => {
    if (request.url === '/') {
        if (request.headers.authorization && 
            request.headers.authorization.split(' ')[0] === 'Bearer') {
            const token = request.headers.authorization.split(' ')[1]
            const secret = config.SECRET_KEY
            jwt.verify(token, secret, next, (error, user) => {
                if (error) {
                    response.sendStatus(403)
                }
                else {
                    return response.sendStatus(200)
                }
            })
        }
        else {
            return response.sendStatus(403)
        }
        
    }
    else if (request.url === '/auth/') {
        next()
    }
    else {
        if (request.headers.authorization && 
            request.headers.authorization.split(' ')[0] === 'Bearer') {
            const token = request.headers.authorization.split(' ')[1]
            const secret = config.SECRET_KEY
            jwt.verify(token, secret, next, (error, user) => {
                if (error) {
                    response.sendStatus(403)
                }
                request.user = user
                next()
            })
        }
        else {
            response.sendStatus(401)
        }
    }
}

export default checkJWT