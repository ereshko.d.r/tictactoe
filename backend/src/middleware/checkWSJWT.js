import { jwt } from '../../App.js'
import config from '../../config.js'

/**
 * middleware that checks `JWT` with incoming request for connecting
 * 
 * @param {*} socket 
 * @param {*} next 
 */

const checkWSJWT = (socket, next) => {
    const token = socket.handshake.auth.accessToken
    if (!token) {
        socket.disconnect()
    }
    const secret = config.SECRET_KEY
    jwt.verify(token, secret, next, (error, user) => {
        if (error) {
            socket.disconnect()
        }
        else {
            socket.data.user = user
            next()
        }
    })
}

export default checkWSJWT