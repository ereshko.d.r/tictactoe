# Bakcend приложения TicTacToe

## Описание приложения

Backend `express.js` приложения для TicTacToe

Текущий URL: `http://178.72.89.213:5100`

## Архитектура приложения

### App.js

Точка входа в приложение

### ./src/modules

Содержит базовый класс для обработки входящих запросов `baseHandlers.js`

### ./src/routers

#### ./router.js

Роутер для приложения

#### /game

Содержит обработчик запросов для игрового поля `gameHandler.js`
