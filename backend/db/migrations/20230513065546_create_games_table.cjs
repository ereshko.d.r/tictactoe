/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('games', (table) => {
            table.increments('id').primary();
            table.integer('player1_id').unsigned().notNullable();
            table.integer('player2_id').unsigned().notNullable();
            table.boolean('game_result').comment('true - 1st player, false - 2nd player, null - draw');
            table.timestamp('started_at', { useTz: false })
            table.timestamp('ended_at', { useTz: false })

            table.foreign('player1_id').references('id').inTable('users').onDelete('cascade').onUpdate('cascade');
            table.foreign('player2_id').references('id').inTable('users').onDelete('cascade').onUpdate('cascade');
        })
};

exports.down = function(knex) {
    return knex.schema
        .dropTable('games')
};
