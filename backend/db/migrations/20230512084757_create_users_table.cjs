/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('users', (table) => {
            table.increments('id').primary();
            table.string('role').checkIn(['admin', 'user']).defaultTo('user').notNullable()
            table.string('last_name').notNullable();
            table.string('first_name').notNullable();
            table.string('second_name').notNullable();
            table.timestamp('birthdate', { useTz: false }).notNullable()
            table.boolean('gender').notNullable().comment('false - female; true - male');
            table.boolean('status_active').defaultTo(true).notNullable();
            table.timestamp('created_at', { useTz: false }).defaultTo(knex.fn.now()).notNullable();
            table.timestamp('updated_at', { useTz: false }).defaultTo(knex.fn.now()).notNullable();
        })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema
        .dropTable('users')
};
