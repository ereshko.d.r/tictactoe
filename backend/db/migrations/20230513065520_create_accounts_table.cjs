/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('accounts', (table) => {
            table.primary(['user_id', 'login']);
            table.integer('user_id').unique().unsigned().notNullable();
            table.string('login').unique().notNullable();
            table.string('password').notNullable();

            table.foreign('user_id').references('id').inTable('users').onDelete('cascade').onUpdate('cascade');
        })
};

exports.down = function(knex) {
    return knex.schema
        .dropTable('accounts')
};
