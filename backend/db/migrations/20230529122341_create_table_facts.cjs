/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
    .createTable('counters', (table) => {
        table.increments('id').primary()
        table.integer('user_id').unsigned().notNullable()
        table.string('symbol', 1).notNullable()
        table.timestamp('month', { useTz: false }).notNullable()
        table.integer('wins_count').defaultTo(0).notNullable()
        table.integer('loses_count').defaultTo(0).notNullable()
        table.integer('draws_count').defaultTo(0).notNullable()
        table.foreign('user_id').references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE')
        table.unique(['user_id', 'symbol', 'month'])
    })
};

exports.down = function(knex) {

};