/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('messenger', (table) => {
            table.integer('game_id').unsigned().notNullable();
            table.integer('user_id').unsigned().notNullable();
            table.text('message').notNullable();
            table.timestamp('cerated_at', { useTz: false }).defaultTo(knex.fn.now()).notNullable();

            table.foreign('game_id').references('id').inTable('games').onDelete('cascade').onUpdate('cascade');
            table.foreign('user_id').references('id').inTable('users').onDelete('cascade').onUpdate('cascade');
        })
};

exports.down = function(knex) {
    return knex.schema
        .dropTable('messenger')
        
};
