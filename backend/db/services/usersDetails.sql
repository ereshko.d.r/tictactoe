select  user_id, 
        month,
        symbol,
        sum(wins_count + loses_count + draws_count) as total_games,
        sum(wins_count) as wins_count,
        sum(loses_count) as loses_count,
        sum(draws_count) as draws_count
FROM rating
GROUP BY ROLLUP(user_id, symbol, month)
ORDER BY user_id, month, symbol