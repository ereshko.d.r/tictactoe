const config = require('../knexfile.cjs')
const knex = require('knex')(config.production)
const bcrypt = require('bcrypt')


/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
const checkIsExsist = async (knex) => {
    return knex('users').join('accounts', {'users.id': 'accounts.user_id'})
        .select('login')
        .where('login', 'testuser')
        .then(data => {
            return (data.length > 0)
        })
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
const createSuperuser = async (knex) => {
    const check = await checkIsExsist(knex)
    if (check) {
        console.log('Superuser already exsist')
        return process.exit(1)
    }
    knex.transaction((trx) => {
        knex('users').transacting(trx).insert({last_name: 'Тестов', first_name: 'Юзер', second_name: 'Куэевичович', birthdate: '2002-09-17T08:18:09.853Z', gender: true, role: 'admin'}).returning('id')
            .then(async (id) => {
                const pass = await bcrypt.hash('testpass', 10)
                return knex('accounts').transacting(trx).insert({user_id: id[0].id, login: 'testuser', password: pass})
            })
            .then(trx.commit)
            .then(() => {
                console.log('superuser has been created: \nlogin:testuser\npass:testpass')
                return process.exit(1)
            })
            .catch(trx.rollback)
        
    })
}

createSuperuser(knex)
