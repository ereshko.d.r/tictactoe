const bcrypt = require('bcrypt')
const { fakerRU } = require('@faker-js/faker')

const MINUTE = 60000
const HOUR = MINUTE * 60
const DAY = HOUR * 24

const DATE_BEGIN = '2022-05-01'
const DATE_END = '2023-05-31'
const MONTH_RANGE = ['2022-12', '2023-01', '2023-02', '2023-03', '2023-04', '2023-05']

const START_OF_DESEMBER = new Date('2022-12-01').getTime()
const END_OF_MAY = new Date('2023-05-31').getTime()
const START_OF_MAY_2022 = new Date('2022-05-01')
const END_OF_NOVEMBER_2022 = new Date('2022-11-30')

const TOTAL_USERS = 2000
const COUNT_GAMES = 1000

const regularUsers = []                                 // ~70%
const specialUsers = []                                 // ~30% 

const usersWhoPlayedFromMayToNovember = []             // ~10%
const usersWhoPlayedALotAMonthFromJanuaryToJune = []    // ~10%
const usersWhoMissedMonthFromJanuaryToJune = []         // ~10%

const createRandomUser = () => {
    const gender = fakerRU.person.sex()
    return {
        first_name: fakerRU.person.firstName(gender),
        last_name: fakerRU.person.lastName(gender),
        second_name: fakerRU.person.middleName(gender),
        role: fakerRU.datatype.boolean(0.9) ? 'user' : 'admin',
        birthdate: fakerRU.date.birthdate({min: 18, max: 50, mode: 'age'}),
        gender: (gender === 'male') ? true : false
    }
}

const addGameFromMayToNovember = (player1) => {
    const date = fakerRU.date.between({ from: START_OF_MAY_2022, to: END_OF_NOVEMBER_2022})
    let player2
    if (date.getTime() >= START_OF_MAY_2022 && date.getTime() <= END_OF_NOVEMBER_2022) {
        const users = regularUsers.concat(usersWhoPlayedALotAMonthFromJanuaryToJune)
        let secondPlayer = fakerRU.helpers.arrayElement(users)
        player2 = (secondPlayer === player1) ? fakerRU.helpers.arrayElement(users) : secondPlayer
    }
    else {
        player2 = fakerRU.helpers.arrayElement(regularUsers)
    }
    const [player1_id, player2_id] = fakerRU.datatype.boolean(0.5) ? [player1, player2] : [player2, player1]
    const started_at = date
    const ended_at = fakerRU.date.between( {from: date, to: new Date(date.getTime() + 10 * MINUTE)})
    const game_result = (fakerRU.datatype.boolean(0.7)) ? (fakerRU.datatype.boolean(0.5)) : null
    return {
        player1_id,
        player2_id,
        game_result,
        started_at,
        ended_at
    }
}

const addGameNormalRange = (player1) => {
    const date = fakerRU.date.between({ from: DATE_BEGIN, to: DATE_END})
    let player2
    if (date.getTime() >= START_OF_DESEMBER && date.getTime() <= END_OF_MAY) {
        const users = regularUsers.concat(usersWhoPlayedALotAMonthFromJanuaryToJune)
        let secondPlayer = fakerRU.helpers.arrayElement(users)
        player2 = (secondPlayer === player1) ? fakerRU.helpers.arrayElement(users) : secondPlayer
    }
    else {
        let secondPlayer = fakerRU.helpers.arrayElement(regularUsers)
        player2 = (secondPlayer === player1) ? fakerRU.helpers.arrayElement(regularUsers) : secondPlayer
    }
    const [player1_id, player2_id] = fakerRU.datatype.boolean(0.5) ? [player1, player2] : [player2, player1]
    const started_at = date
    const ended_at = fakerRU.date.between( {from: date, to: new Date(date.getTime() + 10 * MINUTE)})
    const game_result = (fakerRU.datatype.boolean(0.7)) ? (fakerRU.datatype.boolean(0.5)) : null
    return {
        player1_id,
        player2_id,
        game_result,
        started_at,
        ended_at
    }
}

const addGameWhithoutSomeMonth = (player1, month) => {
    let date
    if (month.getMonth() != new Date(START_OF_DESEMBER).getMonth() && month.getMonth() != new Date(END_OF_MAY).getMonth()) {
        date = fakerRU.helpers.arrayElement([
            fakerRU.date.between({ from: START_OF_DESEMBER, to: new Date(month.getTime() - (2 * DAY))}),
            fakerRU.date.between({ from: new Date(month).setMonth(month.getMonth() + 1), to: END_OF_MAY})
        ])
    }
    else if (month.getMonth() === new Date(END_OF_MAY).getMonth()) {
        date = fakerRU.date.between({ from: START_OF_DESEMBER, to: new Date(month.getTime() - (2 * DAY))})
    }
    else {
        date = fakerRU.date.between({ from: new Date(month).setMonth(month.getMonth() + 1), to: END_OF_MAY})
    }
    let player2
    if (date.getTime() >= START_OF_DESEMBER && date.getTime() <= END_OF_MAY) {
        const users = regularUsers.concat(usersWhoPlayedALotAMonthFromJanuaryToJune)
        let secondPlayer = fakerRU.helpers.arrayElement(users)
        player2 = (secondPlayer === player1) ? fakerRU.helpers.arrayElement(users) : secondPlayer
    }
    else {
        player2 = fakerRU.helpers.arrayElement(regularUsers)
    }
    const [player1_id, player2_id] = fakerRU.datatype.boolean(0.5) ? [player1, player2] : [player2, player1]
    const started_at = date
    const ended_at = fakerRU.date.between( {from: date, to: new Date(date.getTime() + 10 * MINUTE)})
    const game_result = (fakerRU.datatype.boolean(0.7)) ? (fakerRU.datatype.boolean(0.5)) : null
    return {
        player1_id,
        player2_id,
        game_result,
        started_at,
        ended_at
    }
}

const addGameWhitEmphasisOnMonth = (player1, month) => {
    let date
    if (fakerRU.datatype.boolean(0.6)) {
        date = fakerRU.date.between({ from: month, to: new Date(month.getTime() + DAY * 29)})
    }
    else {
        date = fakerRU.date.between({ from: DATE_BEGIN, to: DATE_END})
    }
    let player2
    if (date.getTime() >= START_OF_DESEMBER && date.getTime() <= END_OF_MAY) {
        const users = regularUsers.concat(usersWhoPlayedALotAMonthFromJanuaryToJune)
        let secondPlayer = fakerRU.helpers.arrayElement(users)
        player2 = (secondPlayer === player1) ? fakerRU.helpers.arrayElement(users) : secondPlayer
    }
    else {
        player2 = fakerRU.helpers.arrayElement(regularUsers)
    }
    const [player1_id, player2_id] = fakerRU.datatype.boolean(0.5) ? [player1, player2] : [player2, player1]
    const started_at = date
    const ended_at = fakerRU.date.between( {from: date, to: new Date(date.getTime() + 10 * MINUTE)})
    const game_result = (fakerRU.datatype.boolean(0.7)) ? (fakerRU.datatype.boolean(0.5)) : null
    return {
        player1_id,
        player2_id,
        game_result,
        started_at,
        ended_at
    }
}

const progress = (precent) => {
    process.stdout.clearLine()
    process.stdout.cursorTo(0)
    process.stdout.write(`Progress: [ ${precent}% ]`)
    if (precent === 100) {
        process.stdout.write('\n')
        console.log('Done!')
    }
}


/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
    await knex('users').del()
    await knex('accounts').del()
    await knex('games').del()
    await knex.transaction((trx) => {
        const START = new Date()
        knex('users').transacting(trx)
            .insert(fakerRU.helpers.multiple(createRandomUser, {count: TOTAL_USERS})).returning({user_id: 'id'})
            .then(async (users_id) => {
                const users = []
                let counter = 0
                console.log('Start to create password for users')
                for (const user of users_id) {
                    const password = await bcrypt.hash(String(user.user_id), 10)
                    user.password = password
                    user.login = `${fakerRU.internet.userName()}:${user.user_id}`
                    users.push(user)
                    counter ++
                    progress(100 * counter / users_id.length)
                }
                return knex('accounts').transacting(trx)
                    .insert(users).returning({id: 'user_id'})
            })
            .then((users) => {
                for (const user of users) {
                    if (fakerRU.datatype.boolean(0.30)) {
                        specialUsers.push(user.id)
                    }
                    else {
                        regularUsers.push(user.id)
                    }
                }
                console.log(`Total "special" users: ${100 * specialUsers.length / TOTAL_USERS}% or ${specialUsers.length} users`)
                console.log(`Total "regular" users: ${100 * regularUsers.length / TOTAL_USERS}% or ${regularUsers.length} users`)
                for (const user of specialUsers) {
                    const putUserToCase = fakerRU.helpers.arrayElement([
                        usersWhoMissedMonthFromJanuaryToJune,
                        usersWhoPlayedALotAMonthFromJanuaryToJune,
                        usersWhoPlayedFromMayToNovember
                    ])
                    putUserToCase.push(user)
                }
                console.log(`The case of "missed month from january to june": ${usersWhoMissedMonthFromJanuaryToJune.length * 100 / TOTAL_USERS}%`)
                console.log(`The case of "played a lot a month from januaty to june": ${usersWhoPlayedALotAMonthFromJanuaryToJune.length * 100 / TOTAL_USERS}%`)
                console.log(`The case of "played from june to december": ${usersWhoPlayedFromMayToNovember.length * 100 / TOTAL_USERS}%`)
            })
            .then(async () => {
                console.log('Start adding game for regular users')
                let counter = 0
                for (const user of regularUsers) {
                    const userGames = fakerRU.helpers.multiple(() => addGameNormalRange(user), {count: COUNT_GAMES})
                    await knex('games').insert(userGames).transacting(trx)
                    counter ++
                    progress(Math.floor(counter * 100 / regularUsers.length))
                }
                counter = 0
                console.log('Starting add game for users Who Missed Month From January To June')
                for (const user of usersWhoMissedMonthFromJanuaryToJune) {
                    const month = new Date(fakerRU.helpers.arrayElement(MONTH_RANGE))
                    const userGames = fakerRU.helpers.multiple(() => addGameWhithoutSomeMonth(user, month), {count: COUNT_GAMES})
                    await knex('games').insert(userGames).transacting(trx)
                    counter ++
                    progress(Math.floor(counter * 100 / usersWhoMissedMonthFromJanuaryToJune.length))
                }
                counter = 0
                console.log('Start adding game for users Who Played A Lot A Month From January To June')
                for (const user of usersWhoPlayedALotAMonthFromJanuaryToJune) {
                    const month = new Date(fakerRU.helpers.arrayElement(MONTH_RANGE))
                    const userGames = fakerRU.helpers.multiple(() => addGameWhitEmphasisOnMonth(user, month), {count: COUNT_GAMES})
                    await knex('games').insert(userGames).transacting(trx)
                    counter ++
                    progress(Math.floor(counter * 100 / usersWhoPlayedALotAMonthFromJanuaryToJune.length))
                }
                counter = 0
                console.log('Start adding game for users Who Played From June To December')
                for (const user of usersWhoPlayedFromMayToNovember) {
                    const userGames = fakerRU.helpers.multiple(() => addGameFromMayToNovember(user), {count: COUNT_GAMES})
                    await knex('games').insert(userGames).transacting(trx)   
                    counter ++
                    progress(Math.floor(counter * 100 / usersWhoPlayedFromMayToNovember.length))
                }
            })
            .then(async () => {
                await knex.raw(`
                CREATE MATERIALIZED VIEW rating AS
                SELECT users.id AS user_id,
                       'X' AS symbol,
                       date_trunc('month', games.started_at) AS month,
                       count(games.id) FILTER (WHERE games.game_result = TRUE) AS wins_count,
                       count(games.id) FILTER (WHERE games.game_result = FALSE) AS loses_count,
                       count(games.id) FILTER (WHERE games.game_result IS NULL) AS draws_count
                FROM users
                    JOIN games ON users.id = games.player1_id
                GROUP BY users.id, date_trunc('month', games.started_at)
                UNION
                SELECT users.id as user_id,
                       'O' AS symbol,
                       date_trunc('month', games.started_at) AS month,
                       count(games.id) FILTER (WHERE games.game_result = FALSE) AS wins_count,
                       count(games.id) FILTER (WHERE games.game_result = TRUE) AS loses_count,
                       count(games.id) FILTER (WHERE games.game_result IS NULL) AS draws_count
                FROM users
                    JOIN games ON users.id = games.player2_id
                GROUP BY users.id, date_trunc('month', games.started_at)
                WITH NO DATA;
        
                CREATE UNIQUE INDEX ON rating(user_id, month, symbol);
                `).transacting(trx)
            })
            .then(() => {
                const END = new Date()
                console.log(`few users from case "without month": ${usersWhoMissedMonthFromJanuaryToJune.slice(0, 5)}`)
                console.log(`few users from case "played a lot": ${usersWhoPlayedALotAMonthFromJanuaryToJune.slice(0, 5)}`)
                console.log(`Time: ${Math.floor((END - START) / 1000)} sec`)
            })
            .then(trx.commit)
            .catch(trx.rollback)
    })
};
