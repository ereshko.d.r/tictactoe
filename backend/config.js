import * as dotenv from 'dotenv'
dotenv.config()

class Config {
    SECRET_KEY =    process.env.SECRET_KEY

    PORT =          process.env.BACKEND_PORT
    NODE_ENV =      process.env.NODE_ENV

    DB_HOST =       process.env.DB_HOST
    DB_PORT =       process.env.DB_PORT
    DB_USER =       process.env.DB_USER
    DB_PASSWORD =   process.env.DB_PASS
    DB_NAME =       process.env.DB_NAME
}

export default new Config();