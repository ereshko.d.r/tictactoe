const fs = require("fs");
const os = require("os");

const crypto = require('crypto')

const setEnvValue = (key, value) => {

    const ENV_VARS = fs.readFileSync("./.env", "utf8").split(os.EOL);
    const target = ENV_VARS.indexOf(ENV_VARS.find((line) => {
        return line.match(new RegExp(key));
    }));
    ENV_VARS.splice(target, 1, `${key}=${value}`);
    fs.writeFileSync("./.env", ENV_VARS.join(os.EOL));
}

const token = crypto.randomBytes(64).toString('hex')
setEnvValue('TOKEN', token)