import express from 'express'
import router from './src/routers/router.js'
import bodyParser from 'body-parser'
import { createRequire } from "module"
import config from './config.js'
import Clients from './src/modules/clients.js'

const require = createRequire(import.meta.url)
const knexfile = require('./db/knexfile.cjs')

export const knex = require('knex')(knexfile['production'])
knex.raw(`REFRESH MATERIALIZED VIEW rating`)
console.log(config.NODE_ENV)
export const bcrypt = require('bcrypt')
export const jwt = require('jsonwebtoken')
const http = require('http')

const app = express()
const PORT = config.PORT

export const listeningСlients = new Clients()

const server = http.createServer(app)
const  { Server } = require('socket.io')
export const ws = new Server(server, {
    cors: {
        origin: "http://localhost:3000",
        methods: ["GET", "POST"],
        allowedHeaders: ["*"],
        credentials: true
    }
})
const cors = require('cors')

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(cors())

app.use(bodyParser.urlencoded({extended: true})); 
app.use(bodyParser.json());


app.use('/api/v1', router)

server.listen(PORT, () => {
    console.log(`server has been started on ${PORT} port ...`)
})
