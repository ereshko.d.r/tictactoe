import React, { useEffect }  from 'react'
import { BrowserRouter as Router, Routes, Route, Navigate} from 'react-router-dom';
import './App.css'
import Header from './components/modules/header/Header';
import AuthPage from './components/pages/auth/page/AuthPage'
import GameFieldPage from './components/pages/gamefield/page/GameFieldPage';
import RatingPage from './components/pages/rating/page/RatingPage'
import ActivePlayersPage from './components/pages/activeplayers/page/ActivePlayers'
import HistoryPage from './components/pages/history/page/HistoryPage'
import UsersPage from './components/pages/users/page/UsersPage'
import PrivateRoute from './components/modules/privateRoute/PrivateRoute';
import UserContext from './components/modules/context/UserContext';
import { user } from './components/modules/context/UserContext';
import AuthService from './components/services/AuthService';

export const API_URL = 'http://localhost:8000/api/v1/'
export const ES_CONNECTION_TIME = 10 * 60 * 1000


function App() {
    useEffect(() => {
        AuthService.checkJWT()
            .catch((badResponse) => {
                if (badResponse.response.status === 403) {
                    user.clearUser()
                }
            })
    }, [])
    return (
        <UserContext.Provider value={user}>
            <Router>
                <Routes>
                    <Route path='/' element={<Navigate to={'/auth/'}/>}/>
                    <Route path={'/auth/'} element={<AuthPage />} />
                    <Route element={<PrivateRoute user={user}/>}>
                        <Route element={<Header />}>
                            <Route path={'/game/'} element={<GameFieldPage/>}/>
                            <Route path={'/rating/'} element={<RatingPage/>}/>
                            <Route path={'/active/'} element={<ActivePlayersPage/>}/>
                            <Route path={'/history/'} element={<HistoryPage/>}/>
                            <Route path={'/users/'} element={<UsersPage/>} />
                        </Route>
                    </Route>
                </Routes>
            </Router>
        </UserContext.Provider>
    );
}

export default App;
