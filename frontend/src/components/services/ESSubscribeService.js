import { EventSourcePolyfill } from 'event-source-polyfill';
import { API_URL } from '../../App'
import { ES_CONNECTION_TIME } from '../../App'

const EventSource = EventSourcePolyfill


class EventSourceSubscribe {
    createAuthHeader() {
        const user = JSON.parse(localStorage.getItem('user'))
        if (user && user.accessToken) {
            return { Authorization: 'Bearer ' + user.accessToken }
        }
        return {}
    }

    subscribe() {
        const connectionTime = ES_CONNECTION_TIME
        return new EventSource(API_URL + 'events/', {
            headers: this.createAuthHeader(),
            heartbeatTimeout: connectionTime
        })
    }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default new EventSourceSubscribe()