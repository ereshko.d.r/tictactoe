import axios from "axios";
import { API_URL } from "../../App"


/**
 * Service class
 * 
 * Creates requests to backend app and handles response from server
 */

class ApiService {
    createAuthHeader() {
        const user = JSON.parse(localStorage.getItem('user'))
        if (user && user.accessToken) {
            return { Authorization: 'Bearer ' + user.accessToken }
        }
        return {}
    }

    get header() {
        return { headers: this.createAuthHeader() }
    }

    async getUsers() {
        return axios.get(API_URL + 'users/', this.header)
            .then(response => {
                if (response.status === 200) {
                    return response.data
                }
            })
            .catch((error) => {
                console.log(error)
            })
    }

    async getActiveUsers() {
        return axios.get(API_URL + 'active/', this.header)
            .then(response => {
                if (response.status === 200) {
                    return response.data
                }
            })
            .catch((error) => {
                console.log(error)
            })
    }

    async createUser(userData) {
        return axios.post(API_URL + 'users/', userData, this.header)
            .then(response => {
                return response.data
            })
            .catch((error) => {
                console.log(error)
            })
    }

    async blockUser(userData) {
        const id = userData.id
        return axios.get(`${API_URL}users/block/${id}/`, this.header)
            .then(response => {
                return response.data
            })
            .catch((error) => {
                console.log(error)
            })
    }

    async unblockUser(userData) {
        const id = userData.id
        return axios.get(`${API_URL}users/unblock/${id}/`, this.header)
            .then(response => {
                return response.data
            })
            .catch((error) => {
                console.log(error)
            })
    }

    async getRating() {
        return axios.get(API_URL + 'rating/', this.header)
            .then(response => {
                if (response.status === 200) {
                    return response.data
                }
            })
            .catch((error) => {
                console.log(error)
            })
    }
    
    async getHistory() {
        return axios.get(API_URL + 'history/', this.header)
            .then(response => {
                if (response.status === 200) {
                    return response.data
                }
            })
            .catch((error) => {
                console.log(error)
            })
    }

    async sendInvite(user) {
        const userId = user.id
        return axios.get(API_URL + 'game/invite/' + userId, this.header)
            .then(response => {
                return response.data
            })
            .catch((error) => {
                console.log(error)
            })
    }

    async createGame(user1, user2) {
        return axios.post(API_URL + 'game/', [user1, user2], this.header)
            .then(response => {
                return response.data
            })
            .catch((error) => {
                console.log(error)
            })
    }

    async redirectToGame(id) {
        const authHeader = this.header
        return axios.get(API_URL + 'game/', { params: {id: id}, ...authHeader })
            .then(response => {
                console.log(response)
                return response.data
            })
            .catch((error) => {
                console.log(error)
            })
    }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default new ApiService();