import axios from 'axios'
import { API_URL } from '../../App'


class AuthService {
    createAuthHeader() {
        const user = JSON.parse(localStorage.getItem('user'))
        if (user && user.accessToken) {
            return { Authorization: 'Bearer ' + user.accessToken }
        }
        return {}
    }

    async login(login, password) {
        return axios
            .post(API_URL + 'auth/', {login, password})
            .then((response) => {
                return response
            })
    }
    async checkJWT() {
        return axios
            .get(API_URL, {headers: this.createAuthHeader()})
    }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default new AuthService();