import React, { useState, useEffect, useContext } from 'react'
import {Outlet, useNavigate, useLocation} from 'react-router-dom'
import classes from './Header.module.css'
import Logo from '../../images/UI/small-logo.svg'
import LogoutButton from '../../UIs/buttons/normal/logout-button/LogoutButton'
import Tab from '../../UIs/tabs/tab/Tab'
import UserContext from '../context/UserContext'
import UsersContext, { users } from '../context/ActiveUsersContext'



const Header = () => {
    const [currentPage, setCurrentPage] = useState()
    const user = useContext(UserContext)
    const navigate = useNavigate()
    const location = useLocation()

    const logout = () => {
        user.clearUser()
    }

    const redirect = (path) => {
        setCurrentPage(path)
        navigate(path)
    }

    useEffect(() => {
        setCurrentPage(location.pathname)
    }, [location])
    

    return ( 
        <>
            <header className={classes.Header}>
                <div><img src={Logo} alt="" /></div>
                <nav className={classes.Navbar}>
                    <Tab path={'/game/'} currentPage={currentPage} onClick={redirect}>Игровое поле</Tab>
                    <Tab path={'/rating/'} currentPage={currentPage} onClick={redirect}>Рейтинг</Tab>
                    <Tab path={'/active/'} currentPage={currentPage} onClick={redirect}>Активные игроки</Tab>
                    <Tab path={'/history/'} currentPage={currentPage} onClick={redirect}>История игр</Tab>
                    <Tab path={'/users/'} currentPage={currentPage} onClick={redirect}>Список игроков</Tab>
                </nav>
                <LogoutButton onClick={logout}/>
            </header>
            <UsersContext.Provider value={users}>
                <Outlet />
            </UsersContext.Provider>
        </>)
}

export default Header
