import React, { useEffect, useRef} from 'react'
import classes from './InviteMessagesBox.module.css'
import InviteMessage from '../InviteMessage'
import ApiService from '../../../../../services/ApiService'

const InviteMessagesBox = ({invites, setInvites, user, ...props}) => {
    const bottomRef = useRef(null)

    const closeMessage = (id) => {
        setInvites(invites.filter(m => m.id !== id))
    }

    const submitInvite = (recipientUser) => {
        const currentUser = {...user}
        delete currentUser.accessToken
        ApiService.createGame(recipientUser, currentUser)
            .then(() => setInvites([]))
        
    }

    useEffect(() => {
        bottomRef.current?.scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' })
    }, [invites])

    return (
        <div className={classes.Box}>
            <div className={classes.BoxContent}>
                {invites.map((user, index) => {
                    return <InviteMessage user={user} key={index} close={closeMessage} submit={submitInvite}/>
                })}
                <div ref={bottomRef}></div>
            </div>
        </div>
    )
}

export default InviteMessagesBox