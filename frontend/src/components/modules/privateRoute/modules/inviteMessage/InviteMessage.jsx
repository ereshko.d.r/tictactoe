import React from 'react'
import classes from './InviteMessage.module.css'
import Icon from '../../../../images/UI/exit-cross.svg'
import SmallSubmitButton from '../../../../UIs/buttons/small/small-submit-button/SmallSubmitButton'
import ThirdHeader from '../../../../UIs/textheaders/third-header/ThirdHeader'

const InviteMessage = ({user, submit, close, ...props}) => {
    const getName = () => {
        return `${user.first_name} ${user.last_name}`
    }

    setInterval(() => close(user.id), 60 * 1000)
    return (
        <div className={classes.InviteBox}>
            <div className={classes.Header}>
                <ThirdHeader>Приглашение</ThirdHeader>
                <div className={classes.ExitContainer}>
                    <div className={classes.ExitButton} onClick={() => close(user.id)}>
                        <img src={Icon} alt="" />
                    </div>
                </div>
            </div>
            
            <div className={classes.SenderContainer}>
                <div>{getName()}</div>
            </div>
                <SmallSubmitButton onClick={() => submit(user)}>Принять</SmallSubmitButton>
        </div>
    )
}

export default InviteMessage