import React, { useContext, useEffect, useState } from 'react'
import { Outlet, Navigate, useNavigate } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import UsersContext from '../context/ActiveUsersContext'
import InviteMessagesBox from './modules/inviteMessage/messagesBox/InviteMessagesBox'
import EventSourceSubscribe from '../../services/ESSubscribeService'


const PrivateRoute = observer((userData) => {
    const navigate = useNavigate()
    const [listening, setListening] = useState(false)
    const [invites, setInvites] = useState([])
    const users = useContext(UsersContext)
    const currentUser = userData.user.user

    useEffect(() => {
        if (!listening) {
            const events = EventSourceSubscribe.subscribe()
            events.onmessage = (event) => {
                console.log(event)
                const eventData = JSON.parse(event.data)
                if (eventData.users) {
                    users.updateUsers(eventData.users)
                }
                if (eventData.invite) {
                    const user = eventData.invite.id
                    setInvites(invites => {
                        if (!invites.find(element => element.id === user)) {
                            return [...invites, eventData.invite]
                        }
                        return [...invites]
                    })
                }
                if (eventData.redirect) {
                    console.log('123')
                    navigate(`/game/?id=${eventData.redirect}`)
                }
            }
            setListening(true)
            if (events.CLOSED) {
                setListening(false)
            }
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [listening])

    const hasJWT = () => {
        return Boolean(currentUser && currentUser.accessToken)
    }
    return  (
        hasJWT() ? <><Outlet/><InviteMessagesBox invites={invites} setInvites={setInvites} user={currentUser}/></> : <Navigate to={'/auth'}/>
    )
    }
)

export default PrivateRoute