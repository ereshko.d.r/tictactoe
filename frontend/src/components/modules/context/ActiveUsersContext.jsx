import React from "react";
import {makeAutoObservable} from 'mobx'

class Users {
    users = []

    constructor() {
        makeAutoObservable(this)
    }

    updateUsers(data) {
        this.users = data
    }
  
}

export const users = new Users()

const UsersContext = React.createContext(users)

export default UsersContext

