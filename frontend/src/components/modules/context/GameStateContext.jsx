import { createContext } from "react";
import { makeAutoObservable } from "mobx";

class GameState {
    field = [null, null, null, null, null, null, null, null, null]
    isGameOver = false
    currentPlayer = {}
    winner = {}
    combination = []
    emptyFields = 9
    time = 0

    constructor() {
        makeAutoObservable(this)
    }

    update(state) {
        this.field = state.field
        this.isGameOver = state.isGameOver
        this.currentPlayer = state.currentPlayer
        this.winner = state.winner
        this.combination = state.combination
        this.emptyFields = state.emptyFields
        this.time = state.time
    }

    get currrentTime() {
        const seconds = this.time % 60
        const minute = Math.floor(this.time / 60)
        return `${(minute < 10) ? `0${minute}` : minute}:${(seconds < 10) ? `0${seconds}` : seconds}`
    }
}

export const GameContext = createContext(new GameState())
