import React from "react";
import {makeAutoObservable} from 'mobx'

class User {
    user = undefined

    constructor() {
        makeAutoObservable(this)
    }

    setUser(data) {
        localStorage.setItem('user', JSON.stringify(data))
        this.user = data
    }

    clearUser() {
        localStorage.removeItem('user')
        this.user = undefined
    }    
}

export const user = new User()

const UserContext = React.createContext(user.setUser(JSON.parse(localStorage.getItem('user'))))

export default UserContext

