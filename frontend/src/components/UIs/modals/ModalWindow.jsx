import React from 'react'
import classes from './ModalWindow.module.css'

const ModalWindow = ({children, showModal, setShowModal, ...props}) => {
    return (
        <div hidden={!showModal}>
            <div className={classes.ModalWindowBackground} onClick={() => setShowModal(false)} >
                <div className={classes.ModalWindowContainer} onClick={(event) => event.stopPropagation()}>
                    {children}
                </div>
            </div>
        </div>
    )
}

export default ModalWindow