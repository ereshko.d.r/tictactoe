import React from 'react'
import classes from './Tab.module.css'
import ThirdHeader from '../../textheaders/third-header/ThirdHeader'

const Tab = ({path, currentPage, onClick, children, ...props}) => {
    let gamePath = undefined

    if (path === '/game/') {
        const prevGameId = JSON.parse(localStorage.getItem('gameId'))
        if (prevGameId) {
            gamePath = `${path}?id=${prevGameId}`
        }
    }

    return (
        <div 
            className={
                [classes.Tab, (path === currentPage) ? classes.Active : ''].join(' ')
            }
            onClick={() => onClick(gamePath || path)}
        >
            <ThirdHeader>{children}</ThirdHeader>
        </div>
    )
}

export default Tab