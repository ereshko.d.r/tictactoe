import React from 'react'
import classes from './SmallSubmitButton.module.css'

const SmallSubmitButton = ({children, ...props}) => {
  return (
    <button {...props} className={classes.Button}>
        {children}
    </button>
  )
}

export default SmallSubmitButton