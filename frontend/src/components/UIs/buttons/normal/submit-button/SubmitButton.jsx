import React from 'react'
import classes from './SubmitButton.module.css'

const SubmitButton = ({idForm, children, ...props}) => {
  return (
    <button {...props} className={classes.SubmitButton} form={idForm}>
        {children}
    </button>
  )
}

export default SubmitButton