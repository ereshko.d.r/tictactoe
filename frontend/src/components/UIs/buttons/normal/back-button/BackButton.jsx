import React from 'react'
import classes from './BackButton.module.css'

const BackButton = ({block, children, ...props}) => {
  return (
    <button {...props} className={classes.BackButton}>
        {children}
    </button>
  )
}

export default BackButton