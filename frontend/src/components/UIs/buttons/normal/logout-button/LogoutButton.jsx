import React from 'react'
import {ReactComponent as Icon} from '../../../../images/UI/signout-icon.svg'
import classes from './LogoutButton.module.css'

const LogoutButton = ({...props}) => {
  return (
    <button {...props} className={classes.LogoutButton}>
        <Icon />
    </button>
  )
}

export default LogoutButton