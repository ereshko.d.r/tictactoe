import React from 'react'
import Icon from '../../../../images/UI/block-icon.svg'
import classes from './BlockButton.module.css'

const BlockButton = ({block, action, children, ...props}) => {
  return (
    <button {...props} className={classes.BlockButton} onClick={action}>
        {(block) ? <img src={Icon} alt='' /> : null}
        {children}
    </button>
  )
}

export default BlockButton