import React from 'react'
import Female from '../../../images/UI/female.svg'
import Male from '../../../images/UI/male.svg'
import classes from './GenderRadioButton.module.css'

const GenderRadioButton = ({value, onChange, ...props}) => {
    
    return (
        <div className={classes.GenderRadioButtonContainer}>
                <div className={classes.GenderRadioButton}>
                    <input type="radio" name="gender" id="0" onChange={event => onChange(event)}/>
                    <label htmlFor="0"><img src={Female} alt="" /></label>
                </div>
                
                <div className={classes.GenderRadioButton}>
                    <input type="radio" name="gender" id="1" onChange={event => onChange(event)}/>
                    <label htmlFor="1"><img src={Male} alt="" /></label>
                </div>
            </div>
    )
}

export default GenderRadioButton