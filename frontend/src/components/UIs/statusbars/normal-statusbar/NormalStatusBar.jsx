import React from 'react'
import classes from './NormalStatusBar.module.css'

const NormalStatusBar = ({color, children, ...props}) => {
    const getClass = () => {
        switch (color) {
            case 'green':
                return classes.GreenBar
            case 'red':
                return classes.RedBar
            case 'blue':
                return classes.BlueBar
            default:
                return classes.GrayBar
        }
    }
    return (
        <div className={[classes.Bar, getClass()].join(' ')}>
            {children}
        </div>
    )
}

export default NormalStatusBar