import React, { useState } from 'react'
import Cross from '../../images/symbols/large-cross.svg'
import Zero from '../../images/symbols/large-zero.svg'
import classes from './Cell.module.css'

const Cell = ({id, game, ...props}) => {
    const [fillClass, setFillClass] = useState(null)
    const [cursor, setCursor] = useState('pointer')
    const [symbol, setSymbol] = useState(null)
    
    if (game.field[id] !== null && !symbol) {
        setSymbol((game.field[id] === 'x') ? Cross : Zero)
        setCursor('default')
    }

    if (symbol && game.emptyFields === 9) {
        setFillClass(null)
        setCursor('pointer')
        setSymbol(false)
    }

    if (game.combination && !fillClass) {
        if (game.combination.includes(id)) {
            setFillClass((game.winner.symbol === 'x') ? classes.Cross : classes.Zero)
        }
    }

    if (game.isGameOver && cursor !== 'default') {
        setCursor('default')
    }

    return (
        <div className={[classes.Cell, fillClass].join(' ')} style={{cursor: cursor}} id={id} {...props}>
            {(symbol) ? <img src={symbol} alt="" /> : null}
        </div>
    )
}


export default Cell
