import React from 'react'
import classes from './FirstHeader.module.css'

const FirstHeader = ({children, ...props}) => {
  return (
    <h1 className={classes.FirstHeader}>
        {children}
    </h1>
  )
}

export default FirstHeader