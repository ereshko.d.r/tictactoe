import React from 'react'
import classes from './ThirdHeader.module.css'

const ThirdHeader = ({children, ...props}) => {
  return (
    <h3 className={classes.ThirdHeader}>
        {children}
    </h3>
  )
}

export default ThirdHeader