import React from 'react'
import classes from './Input.module.css'

const Input = ({type, isError, idForm, onChange, value, ...props}) => {
    const getParams = () => {
        switch (type) {
            case 'login':
                return {
                    type: 'text',
                    placeholder: 'Логин',
                    name: 'login',
                    onCopy: (event) => {event.preventDefault()},
                    onCut: (event) => {event.preventDefault()}
                }
            case 'password':
                return {
                    type: 'password',
                    placeholder: 'Пароль',
                    name: 'password',
                    onCopy: (event) => {event.preventDefault()},
                    onCut: (event) => {event.preventDefault()}
                }
            default:
                break;
        }
    }
    return (
        <div className={classes.InputContainer}>
            <input 
                className={
                    [classes.Input, (isError) ? classes.ErrorBorder : ''].join(' ')
                }
                {...getParams()}
                form={idForm}
                onChange={onChange}
                value={value}
                {...props}
            />
            <div className={classes.ErrorMessage} hidden={!isError}>
                Неверный&nbsp;{(type === 'login' ? 'логин' : 'пароль')}
            </div>
        </div>
    )
}

export default Input