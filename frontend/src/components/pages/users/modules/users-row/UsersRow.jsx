import React, { useState } from 'react'
import Male from '../../../../images/UI/male.svg'
import Female from '../../../../images/UI/female.svg'
import NormalStatusBar from '../../../../UIs/statusbars/normal-statusbar/NormalStatusBar'
import BlockButton from '../../../../UIs/buttons/normal/block-button/BlockButton'
import classes from './UsersRow.module.css'
import ApiService from '../../../../services/ApiService'

const UsersRow = ({user, isAdmin, ...props}) => {
    const [activeStatus, setActiveStatus] = useState(user.status_active)
    const [disabled, setDisabled] = useState(false)

    const getFullName = () => {
        return `${user.last_name} ${user.first_name} ${user.second_name}`
    }

    const getAge = () => {
        const diff = Number(new Date().getFullYear()) - Number(new Date(new Date(user.birthdate).getFullYear()))
        
        return diff
    }

    const getGenderIcon = () => {
        if (user.gender) {
            return <img src={Male} alt='' width='24px'/>
        }
        return <img src={Female} alt='' width='24px'/>
    }

    const getStatusBar = () => {
        if (!activeStatus) {
            return <NormalStatusBar color={'red'}>Заблокирован</NormalStatusBar>
        }
        return <NormalStatusBar color={'green'}>Активен</NormalStatusBar>
    }

    const getTime = (time) => {
        const option = {day: 'numeric', month: 'long', year: 'numeric'}
        return new Date(time).toLocaleDateString('ru-RU', option).slice(0, -3)
    }

    const changeStatus = () => {
        if (activeStatus) {
            setDisabled(true)
            ApiService.blockUser(user)
                .then((data) => {
                    setDisabled(false)
                    setActiveStatus(data.status_active)
                })
            
        }
        else {
            setDisabled(true)
            ApiService.unblockUser(user)
                .then((data) => {
                    setDisabled(false)
                    setActiveStatus(data.status_active)
                })
        }
    }

    return (
        <div className={classes.UsersRow}>
            <div>{getFullName()}</div>
            <div>{getAge()}</div>
            <div>{getGenderIcon()}</div>
            <div className={classes.NormalStatusBar}>{getStatusBar(activeStatus)}</div>
            <div>{getTime(user.created_at)}</div>
            <div>{getTime(user.updated_at)}</div>
            {
                (isAdmin) ? 
                <div className={classes.BlockButtons}>
                    {(activeStatus) ?
                    <BlockButton disabled={disabled} action={changeStatus} block>Заблокировать</BlockButton> :
                    <BlockButton disabled={disabled} action={changeStatus}>Разблокировать</BlockButton>
                    }
                </div> : <></>
            }
            
        </div>
    )
}

export default UsersRow