import React from 'react'
import classes from './UsersContent.module.css'
import ThirdHeader from '../../../../UIs/textheaders/third-header/ThirdHeader'

const UsersContent = ({children, ...props}) => {
  return (
    <div className={classes.UsersContent}>
        <div className={classes.Table}>
            <ThirdHeader>ФИО</ThirdHeader>
            <ThirdHeader>Возраст</ThirdHeader>
            <ThirdHeader>Пол</ThirdHeader>
            <ThirdHeader>Статус</ThirdHeader>
            <ThirdHeader>Создан</ThirdHeader>
            <ThirdHeader>Изменен</ThirdHeader>
            <ThirdHeader></ThirdHeader>
        </div>
        {children}
    </div>
  )
}

export default UsersContent