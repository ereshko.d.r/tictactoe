import React, { useState } from 'react'
import Icon from '../../../../images/UI/exit-cross.svg'
import classes from './UserModalContent.module.css'
import GenderRadioButton from '../../../../UIs/switches/male-female-radio-button/GenderRadioButton'
import FirstHeader from '../../../../UIs/textheaders/first-header/FirstHeader'
import ThirdHeader from '../../../../UIs/textheaders/third-header/ThirdHeader'
import Input from '../../../../UIs/inputs/Input'
import SubmitButton from '../../../../UIs/buttons/normal/submit-button/SubmitButton'
import ApiService from '../../../../services/ApiService'

const UserModalContent = ({setShowModal, update}) => {
    const [name, setName] = useState('')
    const [age, setAge] = useState('')
    const [gender, setGender] = useState('')
    const disabled = (name.split(' ').length === 3 && age && gender) ? false : true

    const changeInput = (event) => {
        switch (event.target.name) {
            case 'name':
                setName(event.target.value.replace(/([\d])/g, ''))
                break;
            case 'age':
                setAge(event.target.value.replace(/[^\d]/g, ''))
                break;
            case 'gender':
                setGender(event.target.id)
                break;
            default:
                break;
        }
    }

    const submitData = () => {
        const splitedName = name.split(' ')
        const userData = {
            last_name: splitedName[0],
            first_name: splitedName[1],
            second_name: splitedName[2],
            birthdate: new Date().setFullYear(new Date().getFullYear() - age),
            gender: gender
        }
        ApiService.createUser(userData)
            .finally(() => {
                setAge('')
                setName('')
                setGender('')
                setShowModal(false)
            })
    }

    return (
        <div className={classes.UserModalContent}>
            <div className={classes.CrossExitContainer}>
                <div className={classes.CrossExit}>
                    <img src={Icon} alt="" onClick={() => setShowModal(false)}/>
                </div>
            </div>
            <FirstHeader>Добавьте игрока</FirstHeader>
            <div className={classes.SubjectName}>
                <ThirdHeader>ФИО</ThirdHeader>
                <Input placeholder={'Фамилия Имя Отчество'} name={'name'} onChange={changeInput} value={name}/>
            </div>
            <div className={classes.SubjectProperty}>
                <div className={classes.SubjectAge}>
                    <ThirdHeader>Возраст</ThirdHeader>
                    <Input placeholder={0} name={'age'} onChange={changeInput} value={age}/>
                </div>
                <div className={classes.SubjectSex}>
                    <ThirdHeader>Пол</ThirdHeader>
                    <GenderRadioButton onChange={changeInput}/>
                </div>
            </div>
            <SubmitButton disabled={disabled} onClick={submitData}>Добавить</SubmitButton>
        </div>
    )
}

export default UserModalContent