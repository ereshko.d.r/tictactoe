import React, { useState, useEffect, useContext } from 'react'
import classes from '../style/UsersPage.module.css'
import MainWindow from '../../../UIs/windows/main-window/MainWindow'
import SubmitButton from '../../../UIs/buttons/normal/submit-button/SubmitButton'
import UsersContent from '../modules/users-content/UsersContent'
import UsersRow from '../modules/users-row/UsersRow'
import ModalWindow from '../../../UIs/modals/ModalWindow'
import UserModalContent from '../modules/modal-content/UserModalContent'
import ApiService from '../../../services/ApiService'
import UserContext from '../../../modules/context/UserContext'

const UsersPage = ({props}) => {
    const WINDOW_HEADER = 'Список игроков'
    const user = useContext(UserContext).user
    const isAdmin = (user.role === 'admin') ? true : false

    const [users, setUsers] = useState()
    const [showModal, setShowModal] = useState(false)
    
    const addUserButton = <SubmitButton onClick={() => setShowModal(true)}>
        Добавить игрока
    </SubmitButton>    

    const update = useEffect(() => {
        document.title = WINDOW_HEADER
        ApiService.getUsers()
            .then(data => setUsers(data))
    }, [])

    return (users) ? (
        <div className={classes.Main}>
            <ModalWindow showModal={showModal} setShowModal={setShowModal}>
                <UserModalContent setShowModal={setShowModal} update={update}/>
            </ModalWindow>
            <div className={classes.Content}>
                <MainWindow header={WINDOW_HEADER} headerItems={(isAdmin) ? addUserButton : null}>
                    <UsersContent>
                        {users.map((user, index) => (
                            <UsersRow isAdmin={isAdmin} user={user} key={index} />
                        ))}
                    </UsersContent>
                </MainWindow>
            </div>
        </div>
    ) : null
}

export default UsersPage