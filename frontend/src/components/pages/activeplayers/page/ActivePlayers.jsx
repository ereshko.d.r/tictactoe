import React, { useState, useEffect, useContext } from 'react'
import { observer } from 'mobx-react-lite'
import classes from '../style/ActivePlayers.module.css'
import MainWindow from '../../../UIs/windows/main-window/MainWindow'
import SwitchToggle from '../../../UIs/switches/switch-toggle/SwitchToggle'
import ActivePlayersRow from '../modules/active-players-row/ActivePlayersRow'
import UsersContext from '../../../modules/context/ActiveUsersContext'


const ActivePlayers = ({...props}) => {
    const WINDOW_HEADER = 'Активные игроки'

    useEffect(() => {
        document.title = WINDOW_HEADER
    }, [])
    
    const users = useContext(UsersContext)
    const [filter, setFilter] = useState(false)

    const useFilter = () => {
        (filter) ? setFilter(false) : setFilter(true)
    }

    const ActiveUsers = observer(({users, filter}) => {
        return users.users.filter(user => {
            if (filter) {
                return user.statusGame
            }
            return true
        })
            .map((user, index) => (
            <ActivePlayersRow user={user} key={index}/>
        ))
    })

    return (users) ? (
        <div className={classes.Main}>
            <div className={classes.Content}>
                <MainWindow header={WINDOW_HEADER} headerItems={
                    <div className={classes.SwitchContainer}>
                        <div>Только свободные</div>
                        <SwitchToggle value={filter} onChange={useFilter}/>
                    </div>
                }>
                    <ActiveUsers users={users} filter={filter}/>
                </MainWindow>

            </div>
        </div>
    ) : null
}

export default ActivePlayers