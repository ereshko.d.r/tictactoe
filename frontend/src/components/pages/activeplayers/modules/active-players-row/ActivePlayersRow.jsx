import React, { useContext, useState } from 'react'
import classes from './ActivePlayersRow.module.css'
import NormalStatusBar from '../../../../UIs/statusbars/normal-statusbar/NormalStatusBar'
import BackButton from '../../../../UIs/buttons/normal/back-button/BackButton'
import SubmitButton from '../../../../UIs/buttons/normal/submit-button/SubmitButton'
import ApiService from '../../../../services/ApiService'

import UserContext from '../../../../modules/context/UserContext'

const ActivePlayersRow = ({user, ...props}) => {
    const status = user.status_game
    const [disabled, setDisabled] = useState(false)

    const currentUser = useContext(UserContext)
    
    const getFullName = (player) => {
        return `${player.last_name} ${player.first_name} ${player.second_name}`
    }

    const getStatusBar = (status) => {
        if (status) {
            return <NormalStatusBar color={'green'}>Свободен</NormalStatusBar>
        }
        else if (!status) {
            return <NormalStatusBar color={'blue'}>В игре</NormalStatusBar>
        }
        return <NormalStatusBar>Вне игры</NormalStatusBar>
    }

    const sendInvite = () => {
        setDisabled(true)
        ApiService.sendInvite(user)
            .then(setDisabled(false))
    }

    const getButton = (status, disabled) => {
        if (!status || currentUser.user.id === user.id) {
            return <BackButton disabled>Позвать играть</BackButton>
        }
        return <SubmitButton disabled={disabled} onClick={sendInvite}>Позвать играть</SubmitButton>
    }

    return (
        <div className={classes.ActivePlayersRow}>
            <div>{getFullName(user)}</div>
            <div className={classes.ActivePlayersRowStatus}>
                <div className={classes.StatusBar}>{getStatusBar(status)}</div>
                <div>{getButton(status, disabled)}</div>
            </div>
        </div>
    )
}

export default ActivePlayersRow