import React from 'react'
import { useNavigate } from 'react-router-dom'
import classes from './ModalContent.module.css'
import Cup from '../../../../images/UI/cup.svg'
import FirstHeader from '../../../../UIs/textheaders/first-header/FirstHeader'
import SubmitButton from '../../../../UIs/buttons/normal/submit-button/SubmitButton'
import BackButton from '../../../../UIs/buttons/normal/back-button/BackButton'

const ModalContent = ({winner, restart, closeSession, ...props}) => {
    const navigate = useNavigate()
    const getText = () => {
        if (winner === 'draw') {
            return 'Ничья!'
        }
        if (winner) {
            return `${winner.last_name} ${winner.first_name} победил!`
        }
    }

    const backButtonAction = () => {
        closeSession()
        navigate('/active/')
    }
    return (
        <div className={classes.ModalContent}>
            <img src={Cup} alt="" width={'132px'}/>
            <FirstHeader>{getText()}</FirstHeader>
            <div className={classes.Buttons}>
                <SubmitButton onClick={restart}>Новая игра</SubmitButton>
                <BackButton onClick={backButtonAction}>Выйти в меню</BackButton>
            </div>
        </div>
    )
}

export default ModalContent