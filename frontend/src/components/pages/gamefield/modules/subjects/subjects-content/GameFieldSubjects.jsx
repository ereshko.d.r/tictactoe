import React from 'react'
import classes from './GameFieldSubjects.module.css'
import FirstHeader from '../../../../../UIs/textheaders/first-header/FirstHeader'
import GameFieldSubjectRow from '../subject-row/GameFieldSubjectRow'

const GameFieldSubjects = ({users, ...props}) => {
    return (
        <div className={classes.GameFieldSubjects}>
            <FirstHeader>Игроки</FirstHeader>
            <div className={classes.GameFieldSubjectsContent}>
                <GameFieldSubjectRow user={users[0]} />
                <GameFieldSubjectRow user={users[1]} />
            </div>
        </div>
    )
}

export default GameFieldSubjects