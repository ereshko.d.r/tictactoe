import React, {useEffect, useRef, useState } from 'react'
import classes from './ChatModule.module.css'
import ChatBox from '../chat-box/ChatBox'
import ChatInput from '../chat-input/ChatInput'


const ChatModule = ({user, websocket, ...props}) => {
    const [messageList, setMessages] = useState([])
    const [chatInput, setChatInput] = useState('')
    
    const bottomRef = useRef(null)
    
    const changeMessage = (event) => {
        setChatInput(event.target.value)
    }
    
    const pushMessageList = (message) => {
        setMessages((messages) => [...messages, message])
    }
    
    const sendMessage = (event) => {
        event.preventDefault()
        const currentTime = new Date().toTimeString().split(':')
        if (chatInput) {
            const newMessage = {
                user: user,
                time: `${currentTime[0]}:${currentTime[1]}`,
                text: chatInput,
            }
            websocket.current.emit('message', JSON.stringify(newMessage))
            setChatInput('')
        }
    }

    useEffect(() => {
        websocket.current.on('message', (message) => {
            pushMessageList(JSON.parse(message))
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    useEffect(() => {
        bottomRef.current?.scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });
    }, [messageList])

    return (
        <div className={classes.ChatModule}>
            <div className={classes.MessageHiderTop}></div>
            
            <div className={classes.ChatContent}>
                {(messageList.length !== 0) ? messageList.map((message, index) => {
                    const currentSymbol = message.user.symbol
                    
                    let prevSymbol
                    if (index > 0) {
                        prevSymbol = messageList[index - 1].user.symbol
                    }
                    return <ChatBox {...message} same={currentSymbol === prevSymbol} key={index}/>
                }) : <div className={classes.EmptyBox}>
                        Сообщений еще нет
                    </div>}
                <div ref={bottomRef}></div> {/* Якорь для скроллинга к последнему сообщению */}
            </div>
            
            <div className={classes.ChatInput}>
            <div className={classes.MessageHiderBottom}></div>
                <ChatInput value={chatInput} onChange={changeMessage} submit={sendMessage}/>
            </div>
        </div>
    )
}

export default ChatModule