import React from 'react'
import classes from './ChatBox.module.css'

const ChatBox = ({user, time, text, same, ...props}) => {
    return (
        <div className={
            [
                classes.ChatBox,
                (user.symbol === 'x') ? classes.Cross : classes.Zero,
                (same) ? classes.Same : ''
            ].join(' ')
        }>
            <div {...props} className={classes.ChatBoxHeader}>
                <div className={classes.UserName}>
                    {`${user.last_name} ${user.first_name}`}
                </div>
                <div className={classes.Time}>
                    {time}
                </div>
            </div>
            <div className={classes.ChatBoxContent}>
                {text}
            </div>
        </div>
    )
}

export default ChatBox