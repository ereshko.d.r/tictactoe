import React from 'react'
import classes from './ChatInput.module.css'
import SenderButton from '../../../../../UIs/buttons/normal/sender-button/SenderButton'
import Input from '../../../../../UIs/inputs/Input'

const ChatInput = ({value, onChange, submit, ...props}) => {
    
    return (
        <form action='none' className={classes.ChatInput} onSubmit={submit} id='chatForm'>
            <Input idForm={'chatForm'} value={value} onChange={onChange}/>
            <SenderButton form={'chatForm'} onClick={submit} />
        </form>
    )
}

export default ChatInput