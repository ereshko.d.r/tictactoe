import React, { useContext, useEffect, useState } from 'react'
import classes from './GameField.module.css'
import Cell from '../../../../UIs/cells/Cell'
import Cross from '../../../../images/symbols/cross.svg'
import Zero from '../../../../images/symbols/zero.svg'
import ModalWindow from '../../../../UIs/modals/ModalWindow'
import ModalContent from '../modal-content/ModalContent'
import { GameContext } from '../../../../modules/context/GameStateContext'
import { observer } from 'mobx-react-lite'

const UPDATE_STATE = 'update state'
const RESTART = 'restart'


const GameField = ({user, websocket, ...props}) => {
    const [showModal, setShowModal] = useState(false)
    const gameState = useContext(GameContext)
    

    if (!websocket) {
        const prevState = JSON.parse(localStorage.getItem('game'))
        prevState.isGameOver = true
        gameState.update(prevState)
    }

    const getField = () => {
        websocket.current.emit('get state')
    }

    const action = (event) => {
        if (
            !gameState.isGameOver &&
            gameState.currentPlayer.symbol === user.symbol &&
            gameState.field[event.target.id] === null
        ) {
            websocket.current.emit('make move', event.target.id)
        }
        if (gameState.isGameOver) {
            setShowModal(true)
        }
    }

    const restart = () => {
        websocket.current.emit('restart')
        setShowModal(false)
    }

    const CurrentTime = observer(({game}) => <div className={classes.GameFieldTime}>{game.currrentTime}</div>)
    const CellState = observer(({game}) => 
        gameState.field.map((_, index) => {
            return <Cell id={index} onClick={action} game={game} key={index}/>
        })
    )
    const CurrentPlayer = observer(({game}) => {
        const currentPlayer = game.currentPlayer
        const currentSymbol = (currentPlayer.symbol === 'x') ? Cross : Zero
        return (
        <div className={classes.GameFieldStep}>
                <div>Ходит</div>
                <div><img src={currentSymbol} alt="" width='24' /></div>
                <div>{`${currentPlayer.last_name} ${currentPlayer.first_name}`}</div>
        </div>
    )})

    const closeSession = () => {
        websocket.current.disconnect()
        setShowModal(false)
    }
    
    useEffect(() => {
        getField()
        websocket.current.on(UPDATE_STATE, (state) => {
            if (state.isGameOver) {
                setShowModal(true)
            }
            localStorage.setItem('game', JSON.stringify(state))
            gameState.update(state)
        })
        websocket.current.on(RESTART, (state) => {
            gameState.update(state)
            localStorage.setItem('game', JSON.stringify(state))
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (gameState) ? (
        <div className={classes.GameFieldContainer}>
            <CurrentTime game={gameState} />
            <div className={classes.GameFieldBoard}>
            <CellState game={gameState} />
            </div>
            <CurrentPlayer game={gameState} />
            <ModalWindow showModal={showModal} setShowModal={setShowModal}>
                <ModalContent winner={gameState.winner} restart={restart} setShowModal={setShowModal} closeSession={closeSession}/>
            </ModalWindow>
        </div>    
    ) : <div className={classes.GameFieldContainer}></div>

}


export default GameField
