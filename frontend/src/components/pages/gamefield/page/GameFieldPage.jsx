import React, { useContext, useEffect, useRef, useState } from 'react'
import classes from '../style/GameFieldPage.module.css'
import GameFieldSubjects from '../modules/subjects/subjects-content/GameFieldSubjects'
import GameField from '../modules/gamefield/GameField'
import ChatModule from '../modules/chat/chat-module/ChatModule'
import { useLocation, useNavigate } from 'react-router-dom'
import { io } from 'socket.io-client'
import UserContext from '../../../modules/context/UserContext'

const API_URL = 'http://localhost:8000/'

const CONNECT = 'connect'
const USERS = 'users'
const DISCONNECT = 'disconnect'
const CONNECT_ERROR = 'connect_error'



const GameFieldPage = ({...props}) => {
    const [users, setUsers] = useState([])
    const [connected, setConnected] = useState(false)
    const server = useRef(null)
    const currentUser = useContext(UserContext).user
    const navigatge = useNavigate()
    const location = useLocation()

    const copyCurrentUserWithSymbol = (arr) => {
        const user = {...currentUser}
        delete user.accessToken
        if (arr[0].id === user.id) {
            user.symbol = arr[0].symbol
            return user
        }
        else {
            user.symbol = arr[1].symbol
            return user
        }
    }
    
    useEffect(() => {
        document.title = 'Игровое поле'

        if (!connected) {
            const query = location.search.replace('?id=', '')
            if (query) {
                localStorage.setItem('gameId', JSON.stringify(query))
                server.current = io(`${API_URL}${query}`, {
                    auth: {accessToken: currentUser.accessToken}
                })
                server.current.on(CONNECT, () => setConnected(true))
                server.current.on(USERS, ([user1, user2]) => {
                    setUsers([user1, user2])
                })
                server.current.on(DISCONNECT, () => {
                    setConnected(false)
                    navigatge('/active/')
                })
                server.current.on(CONNECT_ERROR, () => {
                    setConnected(false)
                    navigatge('/active/')
                })
            }
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return connected ? (
        <div className={classes.Main}>
            <div className={classes.GameFieldPageContent}>
                <div className={classes.GameSubjects}>
                    <GameFieldSubjects users={users} />
                </div>
                    {(users) ? <GameField user={copyCurrentUserWithSymbol(users)} websocket={server} /> : null}
                <div className={classes.ChatModule}>
                    {(users) ? <ChatModule user={copyCurrentUserWithSymbol(users)} websocket={server}/> : null}
                </div>
            </div>
        </div>
    ) : null
}

export default GameFieldPage
