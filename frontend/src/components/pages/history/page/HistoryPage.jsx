import React, { useEffect, useState } from 'react'
import classes from '../style/HistoryPage.module.css'
import MainWindow from '../../../UIs/windows/main-window/MainWindow'
import HistoryContent from '../modules/history-content/HistoryContent'
import HistoryRow from '../modules/history-row/HistoryRow'
import ApiService from '../../../services/ApiService'

const HistoryPage = ({...props}) => {
    const WINDOW_HEADER = 'История игр'
    const [history, setHistory] = useState()

    useEffect(() => {
        document.title = WINDOW_HEADER
        ApiService.getHistory()
            .then(data => setHistory(data))
    }, [])

    return (history) ? (
        <div className={classes.Main}>
            <div className={classes.Content}>
                <MainWindow header={WINDOW_HEADER}>
                    <HistoryContent>
                        {history.map((users, index) => (
                            <HistoryRow users={users} key={index} />
                        ))}
                    </HistoryContent>
                </MainWindow>
            </div>
        </div>
    ) : null
}

export default HistoryPage