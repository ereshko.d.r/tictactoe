import React from 'react'
import Cross from '../../../../images/symbols/cross.svg'
import Zero from '../../../../images/symbols/zero.svg'
import Cup from '../../../../images/UI/small-cup.svg'
import classes from './HistoryRow.module.css'

const HistoryRow = ({users, ...props}) => {
    console.log(users)
    const first_player = `${users.player1_first_name} ${users.player1_last_name[0]}. ${users.player1_second_name[0]}.`
    const second_player = `${users.player2_first_name} ${users.player2_last_name[0]}. ${users.player2_second_name[0]}.`
    const getDate = () => {
        const option = {day: 'numeric', month: 'long', year: 'numeric'}
        return new Date(users.started_at).toLocaleDateString('ru-RU', option).slice(0, -3)
    }
    const getTime = () => {
        const diff = new Date(new Date(users.ended_at) - new Date(new Date(users.started_at)))
        const min = diff.getMinutes()
        const sec = diff.getSeconds()
        return `${min} мин ${sec} сек`
    }
    return (
        <div className={classes.HistoryRow}>
            <div className={classes.PlayersContainer}>
                <div className={classes.PlayerField}>
                    <img src={Zero} alt="" width='16px'/>
                    {first_player}
                    {(users.game_result) ? <img src={Cup} alt='' width='24px'/> : null}
                </div>
                <div className={classes.Versus}>против</div>
                <div className={classes.PlayerField}>
                    <img src={Cross} alt="" width='16px'/>
                    {second_player}
                    {(users.game_result) ? null : <img src={Cup} alt='' width='24px'/>}
                </div>
            </div>
            <div className={classes.Date}>{getDate(users.started_at)}</div>
            <div className={classes.Time}>{getTime()}</div>
        </div>
    )
}

export default HistoryRow