import React, { useContext, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import DogImage from '../../../images/UI/dog.svg'
import classes from './AuthForm.module.css'
import FirstHeader from '../../../UIs/textheaders/first-header/FirstHeader'
import Input from '../../../UIs/inputs/Input'
import SubmitButton from '../../../UIs/buttons/normal/submit-button/SubmitButton'
import AuthService from '../../../services/AuthService'
import UserContext from '../../../modules/context/UserContext'

const AuthForm = () => {
    const IDFORM = 'AuthForm'
    const user = useContext(UserContext)

    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState(false)

    const navigate = useNavigate()

    const disabled = (login && password) ? false : true

    const changeFields = (event) => {
        if (event.target.name === 'login') {
            setLogin(event.target.value.replace(/([^\w\d\.\_\:])/g, "")) // eslint-disable-line
        }
        if (event.target.name === 'password') {
            setPassword(event.target.value)
        }
        setError(false)
    }

    const submitAuthData = async (event) => {
        event.preventDefault()
        AuthService.login(login, password)
            .then(async (response) => {
                if (response.status === 200) {
                    await user.setUser(response.data)
                    navigate('/active/')
                }
            })
            .then()
            .catch(badResponse => {
                if (badResponse.response.status === 404) {
                    setError(true)
                }
            })        
    }

    return (
        <div className={classes.AuthFormContainer}>
            <div className={classes.AuthFormContent}>
                <img src={DogImage} alt="" />
                <FirstHeader>Войдите в игру</FirstHeader>
                <form className={classes.Form} id={IDFORM} onSubmit={submitAuthData}>
                    <Input
                        type={'login'}
                        isError={error}
                        idForm={IDFORM}
                        onChange={event => changeFields(event)}
                        value={login}
                    />
                    <Input
                         type={'password'}
                         isError={error}
                         idForm={IDFORM}
                         onChange={event => changeFields(event)}
                         value={password}
                    />
                </form>
                <SubmitButton disabled={disabled} idForm={IDFORM}>Войти</SubmitButton>
            </div>
        </div>
    )
}

export default AuthForm