import React, { useEffect, useState } from 'react'
import classes from '../style/RatingPage.module.css'
import MainWindow from '../../../UIs/windows/main-window/MainWindow'
import RatingContent from '../modules/rating-content/RatingContent'
import RatingRow from '../modules/rating-row/RatingRow'
import ApiService from '../../../services/ApiService'


const RatingPage = ({...props}) => {
    const WINDOW_HEADER = 'Рейтинг игроков'
    const [users, setUsers] = useState()

    useEffect(() => {
        ApiService.getRating()
            .then(data => {setUsers(data)})
            
    }, [])
    useEffect(() => {
        document.title = WINDOW_HEADER
    }, [])
    return (users) ? (
        <div className={classes.Main}>
            <div className={classes.Content}>
                <MainWindow header={WINDOW_HEADER}>
                    <RatingContent>
                        {users.map((user, index) => (
                            <RatingRow user={user} key={index}/>
                        ))}
                    </RatingContent>
                </MainWindow>
            </div>
        </div>
    ): null
}

export default RatingPage