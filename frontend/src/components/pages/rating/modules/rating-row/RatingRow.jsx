import React from 'react'
import classes from './RatingRow.module.css'

const RatingRow = ({user, ...props}) => {
    const fullName = `${user.last_name} ${user.first_name} ${user.second_name}`
    const winRate = Math.floor(Number(user.wins_count) / Number(user.total) * 100)
    return (
        <div className={classes.RatingRow}>
            <div>{fullName}</div>
            <div>{user.total}</div>
            <div className={classes.Wins}>{user.wins_count}</div>
            <div className={classes.Loses}>{user.loses_count}</div>
            <div>{winRate}%</div>
        </div>
    )
}

export default RatingRow