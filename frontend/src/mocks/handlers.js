import { rest } from 'msw'
import user from './db/testuser.json'
import users from './db/users.json'

export const handlers = [
    rest.post('/api/auth', async (req, res, ctx) => {
        const data = await req.json().then(data => {return data})
        if (data.login === user.login && data.password === user.password) {
            return res(
                ctx.status(200)
            )
        }
        return res(
            ctx.status(404)
        )
    }),
    rest.get('/api/game/users', (req, res, ctx) => {
        const randomIndex = Math.floor(Math.random() * users.length)
        const player1 = user.user
        const player2 = users[randomIndex]
        player1.symbol = 'x'
        player2.symbol = 'o'
        return res(ctx.json({player1: player1, player2: player2}))
    }),
    rest.post('/api/game', async (req, res, ctx) => {
        const data = await req.json().then(data => {return data})
        const URL_Game = 'ws://localhost:5009'
        const URL_Chat = 'ws://localhost:5010'
        const { WSGameServer, WSChatServer } = require('./mocks_workers')
        try {
            const chat = new WSChatServer(URL_Chat, {...data})
            new WSGameServer(URL_Game, {...data}, chat)
        }
        catch (error) {
            console.log(error)
        }
        return res(ctx.json({urlGame: URL_Game, urlChat: URL_Chat}))
    })
    
]