import { setupWorker } from "msw"
import { handlers } from "./handlers"
import { Server } from 'mock-socket'
import TicTacToeLogic from "../components/scripts/game_logic"

export const worker = setupWorker(...handlers)

export class WSGameServer {
    constructor(url, {player1, player2}, chat) {
        const gameState = sessionStorage.getItem('gameState')
        this.chat = chat
        this.phrases = require('./chatPhrases/phrases.json')
        this.server = new Server(url)
        this.server.on('connection', socket => {
            socket.on('message', (message) => {this.#checkMessage(message)})
            socket.on('close', () => {})
        })
        this.timeID = setInterval(() => {this.#tick()}, 1000)
        this.game = new TicTacToeLogic(player1, player2)
        if (gameState) {
            this.game.gameState = JSON.parse(gameState)
        }
        console.log(this.game)
    }

    #tick() {
        if (this.game.isGameStarted() && !this.game.isGameOver) {
            this.game.time++
            const message = {
                event: 'gameTick',
                time: this.game.time
            }
            this.server.emit('message', JSON.stringify(message))
        }
        if (Math.floor(Math.random() * 10) < 1) {
            const randomPhrases = this.phrases.randomPhrases
            this.chat.sendRandomPhrase(randomPhrases)
        }
        if (this.game.currentPlayer.symbol === 'o' && !this.game.isGameOver) {
            this.makeMoveNPC()
        }
        sessionStorage.setItem('gameState', JSON.stringify(this.game.gameState))
    }

    #checkMessage(data) {
        const message = JSON.parse(data)
        switch (message.event) {
            case 'makeMove':
                this.makeMove(message.index)
                break;
            case 'getState':
                this.gameState()
                break;
            case 'setState':
                break;
            case 'restart':
                this.game.restart()
                this.gameState()
                break;
            default:
                break
        }
    }

    #sendMessage(message) {
        const data = JSON.stringify(message)
        this.server.emit('message', data)
    }

    gameState() {
        const message = {
            event: 'getGameState',
            gameState: this.game.gameState,
            time: this.game.time
        }
        this.#sendMessage(message)
    }

    makeMove(index) {
        const result = this.game.game(index)
        sessionStorage.setItem('gameState', JSON.stringify(this.game.gameState))
        this.#sendMessage({
            event: 'updateGameState',
            gameState: this.game.gameState
        })
        if (result) {
            if (result.winner.symbol === 'x') {
                this.chat.sendRandomPhrase(this.phrases.loosePhrases)
            }
            if (result.winner.symbol === 'o') {
                this.chat.sendRandomPhrase(this.phrases.winPrases)
            }
            if (result === 'draw') {
                this.chat.sendRandomPhrase(this.phrases.drawPhrases)
            }
        }
    }

    makeMoveNPC() {
        const randomIndex = Math.floor(Math.random() * 8)
        if (this.game.field[randomIndex] === null) {
            this.makeMove(randomIndex)
        }
    }
}


export class WSChatServer {
    constructor(url, {player1, player2}) {
        this.server = new Server(url)
        this.player1 = player1
        this.player2 = player2
        this.server.on('connection', socket => {
            socket.on('message', () => {
                const phrases = require('./chatPhrases/phrases.json')
                this.sendRandomPhrase(phrases.randomPhrases)
            })
            socket.on('close', () => {
                this.server.close()
            })
        })
    }

    sendRandomPhrase(arr) {
        const randomIndex = Math.floor(Math.random() * arr.length)
        setTimeout(() => {
            this.sendMessage(arr[randomIndex])
        }, Math.floor(Math.random() * 3) * 1000)
    }

    #makeMessage(message) {
        const currentTime = new Date().toTimeString().split(':')
        const newMessage = {
            user: this.player2,
            time: `${currentTime[0]}:${currentTime[1]}`,
            text: message
        }
        return JSON.stringify(newMessage)
    }

    sendMessage(message) {
        const data = this.#makeMessage(message)
        this.server.emit('message', data)
    }
}
