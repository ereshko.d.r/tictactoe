import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// if (process.env.NODE_ENV === 'development') {
//     const { worker } = require('./mocks/mocks_workers')
//     worker.start()
//     console.log('mock is started')
// }

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <App />
);
